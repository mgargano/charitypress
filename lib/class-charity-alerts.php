<?php

class Charity_Alerts {

	private $alerts = array();
	private $text_domain = 'charitypress';

	/**
	 * Private constructor for singleton
	 */
	protected function __construct() {

	}

	/**
	 * Get instance of singleton
	 * @return static
	 */
	public static function getInstance() {
		static $instance = null;
		if ( null === $instance ) {
			$instance = new static();
		}

		return $instance;
	}

	/**
	 * Initialize the class
	 */
	public function init() {

		$this->attach_hooks();

	}

	/**
	 * Attach hooks
	 */
	public function attach_hooks() {

		add_action( 'admin_notices', array( $this, 'alerts' ), PHP_INT_MAX );

	}

	/**
	 * Build update and error notices
	 */
	public function alerts() {


		if ( ! count( $this->alerts ) || ! current_user_can( 'edit_users' ) ) {
			return;
		}
		foreach ( $this->alerts as $alert ) {

			$alert_text = $alert['alert_text'];
			$type       = $alert['type'];
			$template   = '<div class="%s"><p>' . __( '%s', $this->text_domain ) . '</p></div>';
			printf( $template, $type, $alert_text );


		}

	}

	/**
	 * Add update (or generic, based on $type) notice
	 *
	 * @param        $alert
	 */
	public function add_notice( $alert ) {

		$this->alerts[] = array( 'type' => 'updated', 'alert_text' => $alert );
	}

	/**
	 * Add error notice
	 *
	 * @param $alert
	 */
	public function add_error( $alert ) {

		$this->alerts[] = array( 'type' => 'error', 'alert_text' => $alert );

	}

}

Charity_Alerts::getInstance()->init();
