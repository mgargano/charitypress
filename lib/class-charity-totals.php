<?php

class Charity_Totals {

	const TRANSIENT_NAME = 'charitypress_cache';

	public static function init() {

		add_action( 'profile_update', array( __CLASS__, 'update_totals' ) );
		add_action( 'user_register', array( __CLASS__, 'update_totals' ) );
		add_action( 'deleted_user', array( __CLASS__, 'update_totals' ) );

	}


	public static function rebuild_totals() {

		delete_transient( self::TRANSIENT_NAME );
		$users = array_merge(
			get_users(
				array( 'role' => Charity_Role_Manager::RECURRING_ROLE )
			),
			get_users(
				array(
					'role' => Charity_Role_Manager::ONE_TIME_ROLE
				)
			)
		);

		// initialize our return array

		$totals = array(
			'teams' => array(),
			'users' => array(),
			'total' => 0

		);

		foreach ( $users as $user ) {
			$user_total_donated = (float) get_user_meta( $user->ID, 'total-donated', true );

			// update overall total
			$totals['total'] = $totals['total'] + $user_total_donated;

			//update team totals
			$team_linked_objects = wp_get_object_terms( $user->ID, Charity_Team_User_Taxonomy::TAXONOMY_NAME );
			if ( is_array( $team_linked_objects ) && count( $team_linked_objects ) ) {
				$team_on      = $team_linked_objects[0];
				$team_on_slug = $team_on->slug;
				if ( ! array_key_exists( $team_on_slug, $totals['teams'] ) ) {
					$totals['teams'][ $team_on_slug ] = 0;
				}
				$totals['teams'][ $team_on_slug ] = $totals['teams'][ $team_on_slug ] + $user_total_donated;
			}

			//update user totals
			$totals['users'][ $user->ID ] = $user_total_donated;

		}

		set_transient( self::TRANSIENT_NAME, $totals );

		return $totals;


	}

	public static function get_totals() {

		$value = get_transient( self::TRANSIENT_NAME );

		if ( ! $value ) {
			$value = self::rebuild_totals();
		}

		return $value;

	}


}

