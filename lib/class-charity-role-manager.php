<?php

class Charity_Role_Manager {

	const OPTION_NAME = 'charity_role_manager';
	private static $roles;
	const RECURRING_ROLE = 'pledger';
	const ONE_TIME_ROLE = 'donor';

	/**
	 * Set up the class
	 */
	public function init() {

		self::$roles = array(
			strtolower( self::RECURRING_ROLE ) => array(
				'name'     => ucwords( self::RECURRING_ROLE ),
				'caps'     => array( 'is_' . strtolower( self::RECURRING_ROLE ), 'is_charitypress' ),
				'based_on' => 'subscriber'
			),
			strtolower( self::ONE_TIME_ROLE )  => array(
				'name'     => ucwords( self::ONE_TIME_ROLE ),
				'caps'     => array( 'is_' . strtolower( self::ONE_TIME_ROLE ), 'is_charitypress' ),
				'based_on' => 'subscriber'
			)
		);

		$this->attach_hooks();
	}

	/**
	 * Attach hooks for the class
	 */
	public function attach_hooks() {

		add_action( 'admin_init', array( $this, 'add_role_and_cap' ) );

	}

	/**
	 * Return the array of roles needed to be created
	 * @return mixed
	 */
	public function get_roles() {

		return self::$roles;

	}

	/**
	 * Add roles if necessary, store roles created in an option
	 */
	function add_role_and_cap() {


		$role_manager = get_option( self::OPTION_NAME, array() );

		foreach ( self::$roles as $role => $settings ) {


			if ( ! array_key_exists( $role, $role_manager ) || ! $role_manager[ $role ] || ! get_role( $role ) ) {

				$based_on_role       = get_role( $settings['based_on'] );
				$based_on_capability = $based_on_role->capabilities;

				add_role( $role, $settings['name'], $based_on_capability );


				if ( is_array( $settings['caps'] ) ) {

					$role_object = get_role( $role );
					foreach ( $settings['caps'] as $cap ) {
						$role_object->add_cap( $cap );
					}

				}
				$role_manager[ $role ] = true;

				update_option( self::OPTION_NAME, $role_manager );
			}
		}


	}

}

$charity_role_manager = new Charity_Role_Manager;
$charity_role_manager->init();