<?php


class Charity_Stripe_JS {

	public function init() {

		$this->attach_hooks();

	}

	public function attach_hooks() {

		add_action( 'wp_enqueue_scripts', array( $this, 'enqueues' ) );

	}

	public function enqueues() {

		$cs = new Charity_Stripe;
		$public_key = $cs->get_public_key();

		wp_enqueue_script( 'stripejs', 'https://js.stripe.com/v2/' );
		wp_localize_script( 'stripejs', 'stripejs', array( 'public_key' => $public_key ) );

	}


}


$csj = new Charity_Stripe_JS();

$csj->init();
