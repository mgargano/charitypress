<?php

class Charity_Suggest_Team_Name {


	const NONCE_NAME = 'suggest-team-nonce';
	const OPTION_NAME = 'charitypress_suggested_teams';

	public function init() {

		$this->attach_hooks();

	}

	public function attach_hooks() {

		add_action( 'admin_menu', array( $this, 'add_menu_item' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'admin_enqueue' ) );
	}

	function add_menu_item() {

		$page_title = 'Team Suggestions';
		$menu_title = 'Suggested Teams';
		$capability = 'manage_options';
		$menu_slug  = 'suggest-team';
		$function   = array( $this, 'output_screen' );
		add_management_page( $page_title, $menu_title, $capability, $menu_slug, $function );

	}

	function output_screen() {

		?>
		<div id="message" class="updated fade" style="display:none"></div>

		<div class="wrap suggested-teams">
		<h2><?php echo _x( wp_kses_post( 'Suggested Teams' ), 'suggested teams', 'charitypress' ); ?></h2>
		<div class="suggestions">

		<?php
		$teams = $this->get_suggested_teams();
		if ( count( $teams ) ) {

			wp_nonce_field( self::NONCE_NAME, self::NONCE_NAME ); ?>
			<table class="suggest-team">

			<tr>
				<td>User</td>
				<td>Team Suggested</td>
				<td>Approve</td>
				<td>Deny</td>
				<td>Status</td>
			</tr><?php
			foreach ( $teams as $index => $team ) {
				$user       = get_user_by( 'id', $team['user-id'] );
				$user_name  = $user->data->display_name;
				$user_email = $user->data->user_email;
				$team       = $team['team-name'];
				printf( '<tr data-key="%s"><td>%s (%s)</td><td>%s</td><td><span class="action accept">Accept</span></td><td><span class="action reject">Reject</span></td><td><div class="spinner"></div></td></tr>', $index, $user_name, $user_email, $team );


			}
			?></table><?php
		} else {
			?><p>No teams suggested</p><?php
		}
		?></div><?php

	}

	public function admin_enqueue( $hook ) {

		if ( strpos( $hook, 'profile.php' ) !== false || strpos( $hook, 'tools_page_suggest-team' ) !== false ) {
			wp_enqueue_script( 'suggest-team', plugins_url( '/js/admin/suggestTeam.js', dirname( __FILE__ ) ), array(
				'jquery'
			) );
			wp_enqueue_style( 'suggest-team', plugins_url( '/css/admin/suggestTeam.css', dirname( __FILE__ ) ) );

		}

	}

	public function output_suggest() {
		?>
		<div id="suggest-team-name" style="display:none;">
			<p>
				Suggest a Team Name
			</p>
			<input type="text" name="suggested-team" id="suggested-team" data-user-id="<?php echo get_current_user_id(); ?>" />
			<input type="button" value="Suggest" id="suggest-team-execute">
			<?php wp_nonce_field( self::NONCE_NAME, self::NONCE_NAME ); ?>
			<p class="suggest-status">Status</p>
		</div>

		<p>
			<a href="#TB_inline?width=500&height=300&inlineId=suggest-team-name" class="thickbox">Suggest team name</a>
		</p>
	<?php
	}

	public function user_suggested( $team_name ) {

		$user_id = get_current_user_id();
		$this->add_suggested_team( $team_name, $user_id );

	}

	public function get_suggested_teams() {

		return get_option( self::OPTION_NAME, array() );

	}

	public function add_suggested_team( $team_name, $user_id ) {

		$suggestions = array();
		$teams       = $this->get_suggested_teams();
		foreach ( $teams as $team ) {
			$suggestions[] = esc_html( $team['team-name'] );
		}

		if ( in_array( $team_name, $suggestions ) ) {
			wp_send_json_success( array( 'message' => 'Somebody has already suggested this team name and it is awaiting review' ) );
		}

		$teams[] = array( 'team-name' => esc_html( $team_name ), 'user-id' => absint( $user_id ) );
		$this->update_suggested_teams( $teams );
		wp_send_json_success( array( 'message' => 'Added to the queue for review.' ) );

	}

	public function update_suggested_teams( $teams ) {

		return update_option( self::OPTION_NAME, $teams );

	}

	public function team_action( $type, $index ) {

		if ( ! $type ) {
			wp_send_json_error( array( 'message' => 'Invalid type, refresh and try again' ) );
		}
		$teams = $this->get_suggested_teams();
		if ( 'accept' === $type ) {
			$team = wp_kses_post( $teams[ (int) $index ] );

			$term_exists = get_term_by( 'name', $team['team-name'], Charity_Team_User_Taxonomy::TAXONOMY_NAME );
			if ( $term_exists ) {
				wp_send_json_error( array( 'message' => 'Team already exists' ) );
			}
			$term = wp_insert_term( $team['team-name'], Charity_Team_User_Taxonomy::TAXONOMY_NAME );
			if ( is_wp_error( $term ) ) {
				wp_send_json_error( array( 'message' => 'Error inserting term, try again.' ) );
			}
			$term_id   = $term['term_id'];
			$term      = get_term_by( 'id', $term_id, Charity_Team_User_Taxonomy::TAXONOMY_NAME );
			$term_slug = $term->slug;
			$insertion = wp_set_object_terms( $team['user-id'], $term_slug, Charity_Team_User_Taxonomy::TAXONOMY_NAME );
			clean_object_term_cache( $team['user-id'], Charity_Team_User_Taxonomy::TAXONOMY_NAME );
			if ( is_wp_error( $insertion ) ) {
				wp_send_json_error( array( 'message' => 'Error inserting term, try again.' ) );
			}

			update_user_meta( $team['user-id'], 'is-captain', 'on' );
		}

		unset( $teams[ $index ] );
		$this->update_suggested_teams( $teams );
		wp_send_json_success( array( 'message' => 'Success' ) );


	}


}

$ccs = new Charity_Suggest_Team_Name;
$ccs->init();