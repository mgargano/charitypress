<?php

// @todo abstract out an interface

class Charity_Stripe {

	private function get_keys() {

		$environment = Charity_Settings::get( 'environment' );
		$secret_key  = $publishable_key = - 1;
		if ( 'test' === $environment ) {

			$publishable_key = Charity_Settings::get( 'test_publishable_key' );
			$secret_key      = Charity_Settings::get( 'test_secret_key' );


		} elseif ( 'production' === $environment ) {

			$publishable_key = Charity_Settings::get( 'live_publishable_key' );
			$secret_key      = Charity_Settings::get( 'live_secret_key' );

		}

		return array(
			"secret_key"      => $secret_key,
			"publishable_key" => $publishable_key
		);

	}

	public function set_keys() {

		$stripe = $this->get_keys();
		Stripe::setApiKey( $stripe['secret_key'] );

		return true;

	}

	public function get_public_key() {

		$keys = $this->get_keys();

		return $keys['publishable_key'];


	}

	public function get_private_key() {

		$keys = $this->get_keys();

		return $keys['secret_key'];


	}

	/**
	 * @param $args
	 *
	 * @return stdClass
	 */
	public static function charge( $args ) {

		$charge_amount  = $args['charge_amount'];
		$charge_text    = $args['charge_text'];
		$customer_token = $args['customer_token'];
		$log_note       = $args['log_note'];
		$log_summary    = $args['log_summary'];
		$user_id        = $args['user_id'];
		$user           = get_user_by( 'id', $user_id );
		$user_type      = user_can( $user_id, 'is_donor' ) ? Charity_Role_Manager::ONE_TIME_ROLE : Charity_Role_Manager::RECURRING_ROLE;
		$user_logger    = new Charity_User_Logger( $user );
		$post_logger    = new Charity_Post_Logger;


		try {
			$charge_amount = max( 50, $charge_amount * 100 ); //(in pennies), minimum of 50 cents
			Stripe_Charge::create( array(
				"amount"      => $charge_amount,
				"currency"    => "usd",
				"customer"    => $customer_token,
				"description" => $charge_text
			) );
			$log_note = 'Success - ' . $log_note;

			$log_summary = date( 'm/d/Y  H:i:s', time() + get_option( 'gmt_offset' ) * 3600 ) . ' Successfully (' . $user_type . ') ' . $log_summary;
			$post_logger->log_notice( $log_note, $log_summary );
			$user_logger->log( $log_summary );

			$return_object                = new stdClass();
			$return_object->success       = true;
			$return_object->message       = $log_note;
			$return_object->statusMessage = $log_summary;

			return $return_object;

		} catch ( Exception $ex ) {

			$log_note    = 'Failure - ' . $log_note . '<br>Error message from processor ' . $ex->getMessage();
			$log_summary = date( 'm/d/Y  H:i:s', time() + get_option( 'gmt_offset' ) * 3600 ) . '  (' . $user_type . ') Failured to ' . $log_summary . '<br>Error message from processor ' . $ex->getMessage();
			$post_logger->log_notice( $log_note, $log_summary );
			$user_logger->log( $log_summary );
			$return_object                = new stdClass();
			$return_object->success       = false;
			$return_object->message       = $log_note;
			$return_object->statusMessage = $log_summary;
			$cu = new Charity_User;
			$cu->set_user_inactive($user_id);

			return $return_object;
		}


	}

}