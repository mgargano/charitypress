<?php

class Charity_Subscriber {

	const OPTION_NAME = 'charitypress_subscriber';

	public function add( $email ) {

		$emails = $this->get();
		if ( ! in_array( $email, $emails ) ) {
			$this->email_subscriber( $email );
		} if ( in_array( $email, $emails ) ){
			return 'exists';
		}
		$emails[] = $email;
		$emails   = array_unique( $emails );

		return update_option( self::OPTION_NAME, $emails );

	}

	public function get() {
		return get_option( self::OPTION_NAME, array() );
	}

	public function email_subscriber( $email ) {
		$email_admins = Charity_Settings::get( 'email_admins_user_emails' );
		add_filter( 'wp_mail_content_type', array( __CLASS__, 'set_html_content_type' ) );
		$subject = Charity_Settings::get( 'email_subject_after_user_subscribes' );
		$body    = Charity_Settings::get( 'email_body_after_user_subscribes' );
		if ( $subject && $body ) {

			wp_mail( $email, $subject, $body );
			if ( $email_admins ) {

				$admin_email_addresses = array();
				$users                 = get_users( array( 'role' => apply_filters( 'charitpyress_admin_role', 'administrator' ) ) );
				foreach ( $users as $user ) {
					$admin_email_addresses[] = $user->user_email;
				}
				$admin_email_addresses = apply_filters( 'charitypress_admin_emails', $admin_email_addresses );
				wp_mail( $admin_email_addresses, '(ADMIN COPY) ' . $subject, $body );

			}

		}

		remove_filter( 'wp_mail_content_type', array( __CLASS__, 'set_html_content_type' ) );
	}

	public static function set_html_content_type() {
		return 'text/html';
	}

}