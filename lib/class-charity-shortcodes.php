<?php

class Charity_Shortcodes {

	private $direct_donate_method;
	private $pledge_method;
	private $subscribe_method;
	private $individual_leaderboard_method;
	private $team_leaderboard_method;


	public function init() {

		$this->get_methods();
		$this->attach_hooks();

	}

	public function attach_hooks() {


	}

	public function is_user_logged_in() {

		if ( is_user_logged_in() ) {
			return sprintf( 'You are currently logged in. If you want to sign up for another account please <a href="%s">sign out</a> and you can register', wp_logout_url( get_permalink() ) );
		}

		return false;

	}


	public function get_methods() {

		$this->direct_donate_method          = apply_filters( 'charitypress_direct_donate_form_shortcode_method', array(
			$this,
			'direct_donate_form'
		) );
		$this->pledge_method                 = apply_filters( 'charitypress_pledge_form_shortcode_method', array(
			$this,
			'pledge_form'
		) );
		$this->subscribe_method              = apply_filters( 'charitypress_subscribe_form_shortcode_method', array(
			$this,
			'subscribe_form'
		) );
		$this->individual_leaderboard_method = apply_filters( 'charitypress_individual_leaderboard_form_shortcode_method', array(
			$this,
			'individual_leaderboard'
		) );
		$this->team_leaderboard_method       = apply_filters( 'charitypress_team_leaderboard_form_shortcode_method', array(
			$this,
			'team_leaderboard'
		) );

		$this->setup_shortcodes();

	}

	public function setup_shortcodes() {

		add_shortcode( 'charitypress_direct_donate_form', $this->direct_donate_method );
		add_shortcode( 'charitypress_pledge_form', $this->pledge_method );
		add_shortcode( 'charitypress_subscribe_form', $this->subscribe_method );
		add_shortcode( 'charitypress_individual_leaderboard', $this->individual_leaderboard_method );
		add_shortcode( 'charitypress_team_leaderboard', $this->team_leaderboard_method );


	}


	public function direct_donate_form() {
		ob_start();
		if ( $logged_in_message = $this->is_user_logged_in() ) :
			ob_end_clean();

			return $logged_in_message;
		endif;
		?>
		<form method="POST" class="charitypress-form" id="charitypress-direct-donate-form">
			<?php
			do_action( 'charitypress_before_direct_donate_form' );
			?>
			<div class="message"></div>
			<div class="form-group">

				<div class="form-element-wrap">
					<label for="direct-donate-first-name"><?php echo wp_kses_post( _x( 'First Name', 'form label', 'charitypress' ) ); ?></label>
					<input type="text" id="direct-donate-first-name" class="first-name validate[required]" placeholder="<?php echo wp_kses_post( _x( 'First Name', 'form placeholder', 'charitypress' ) ); ?>">
				</div>
			</div>

			<div class="form-group">

				<div class="form-element-wrap">
					<label for="direct-donate-last-name"><?php echo wp_kses_post( _x( 'Last Name', 'form label', 'charitypress' ) ); ?></label>
					<input type="text" id="direct-donate-last-name" class="last-name validate[required]" placeholder="<?php echo wp_kses_post( _x( 'Last Name', 'form placeholder', 'charitypress' ) ); ?>">
				</div>
			</div>

			<div class="form-group">
				<div class="form-element-wrap">
					<label for="direct-donate-amount"><?php echo wp_kses_post( __( 'Amount to donate', 'charitypress' ) ); ?></label>
					<input type="text" id="direct-donate-amount" class="amount validate[required[min[.01]]]" placeholder="<?php echo wp_kses_post( _x( 'Amount', 'form placeholder', 'charitypress' ) ); ?>">
					<br>
					<small class="input"><?php echo wp_kses_post( __( 'All donations can be made in dollars or cents (.04, .05, etc.)', 'charitypress' ) ); ?></small>
				</div>
			</div>
			<div class="form-group">
				<div class="form-element-wrap">
					<label for="direct-donate-anonymous"><?php echo wp_kses_post( __( 'Make me anonymous', 'charitypress' ) ); ?>
						<br>
					</label>
					<input id="direct-donate-anonymous" type="checkbox" class="anonymous">
				</div>
			</div>
			<div class="form-group">
				<div class="form-element-wrap">
					<label for="charity-team"><?php echo wp_kses_post( _x( 'Team', 'form label', 'charitypress' ) ); ?></label>
					<?php
					$terms = get_terms( Charity_Team_User_Taxonomy::TAXONOMY_NAME, array( 'hide_empty' => false ) );
					if ( ! empty( $terms ) ) {
						?><select class="team" name="charity-team">
						<option value="" selected="selected"><?php echo wp_kses_post( __( '-- No Team --', 'charitypress' ) ); ?></option><?php
						foreach ( $terms as $term ) {
							?>
							<option value="<?php echo esc_attr( $term->slug ); ?>">
								<?php echo esc_html( $term->name ); ?></option>

						<?php
						}
						?></select>
						<strong class="input"><?php echo wp_kses_post( __( 'Join a team or create your own from your profile page.', 'charitypress' ) ); ?></strong><?php
					} /* If there are no team terms, display a message. */ else {
						echo wp_kses_post( __( 'There are no teams available.', 'charitypress' ) );
					} ?>
				</div>
			</div>
			<div class="form-group">
				<div class="estimator">

				</div>
			</div>
			<div class="form-group">
				<div class="form-element-wrap">
					<label for="direct-donate-email"><?php echo wp_kses_post( _x( 'Email Address', 'form label', 'charitypress' ) ); ?></label>
					<input id="direct-donate-email" type="text" class="email validate[required,custom[email]]" placeholder="<?php echo wp_kses_post( _x( 'Email', 'form placeholder', 'charitypress' ) ); ?>">
				</div>
			</div>
			<div class="form-group">
				<div class="form-element-wrap">
					<label for="direct-donate-cc"><?php echo wp_kses_post( _x( 'Credit Card Number', 'form label', 'charitypress' ) ); ?></label>
					<input id="direct-donate-cc" type="text" autocomplete="off" size="20" data-stripe="number" class="cc-number validate[required[creditCard]]" placeholder="<?php echo wp_kses_post( _x( 'Credit Card Number', 'form placeholder', 'charitypress' ) ); ?>">
				</div>
			</div>
			<div class="form-group">
				<div class="form-element-wrap">
					<label for="direct-donate-cc-cvc"><?php echo wp_kses_post( _x( 'Credit Card CVC', 'form label', 'charitypress' ) ); ?></label>
					<input id="direct-donate-cc-cvc" type="text" data-stripe="cvc" autocomplete="off" class="validate[required[number]]" placeholder="<?php echo wp_kses_post( _x( 'Credit Card CVC', 'form placeholder', 'charitypress' ) ); ?>">
				</div>
			</div>
			<div class="form-group">
				<div class="form-element-wrap">
					<label for="direct-donate-cc-expiration-mm"><?php echo wp_kses_post( _x( 'Credit Card Expiration', 'form label', 'charitypress' ) ); ?></label>
					<input maxlength="<?php echo wp_kses_post( _x( '2', 'Maxlength for MM', 'charitypress' ) ); ?>" id="direct-donate-cc-expiration-mm" type="text" class="half validate[required,min[0],max[12]" autocomplete="off" size="2" data-stripe="exp-month" class="required" placeholder="<?php echo wp_kses_post( _x( 'MM', 'form placeholder', 'charitypress' ) ); ?>">
					<input maxlength="<?php echo wp_kses_post( _x( '4', 'Maxlength for YYYY', 'charitypress' ) ); ?>" type="text" class="half validate[required,min[<?php echo date( 'Y' ); ?>]" autocomplete="off" size="4" data-stripe="exp-year" class="required" placeholder="<?php echo wp_kses_post( _x( 'YYYY', 'form placeholder', 'charitypress' ) ); ?>">
				</div>

			</div>
			<input type="hidden" name="type" class="type" value="donor">
			<input type="hidden" name="nonce" class="nonce" value="<?php echo wp_create_nonce( 'charitypress-form-nonce' ); ?>">
			<button type="submit"><?php echo wp_kses_post( _x( 'Donate', 'form button', 'charitypress' ) ); ?></button>

			<?php

			do_action( 'charitypress_after_direct_donate_form' );
			?>
		</form>
		<?php
		return ob_get_clean();

	}

	public function pledge_form() {
		ob_start();
		if ( $logged_in_message = $this->is_user_logged_in() ) :
			ob_end_clean();

			return $logged_in_message;
		endif;
		?>
		<form method="POST" class="charitypress-form" id="charitypress-pledge-form">
			<?php
			do_action( 'charitypress_before_pledge_form' );
			?>
			<div class="message"></div>
			<div class="form-group">

				<div class="form-element-wrap">
					<label for="pledge-first-name"><?php echo wp_kses_post( _x( 'First Name', 'form label', 'charitypress' ) ); ?></label>
					<input type="text" id="pledge-first-name" class="first-name validate[required]" placeholder="<?php echo wp_kses_post( _x( 'First Name', 'form placeholder', 'charitypress' ) ); ?>">
				</div>
			</div>

			<div class="form-group">

				<div class="form-element-wrap">
					<label for="pledge-last-name"><?php echo wp_kses_post( _x( 'Last Name', 'form label', 'charitypress' ) ); ?></label>
					<input type="text" id="pledge-last-name" class="last-name validate[required]" placeholder="<?php echo wp_kses_post( _x( 'Last Name', 'form placeholder', 'charitypress' ) ); ?>">
				</div>
			</div>

			<div class="form-group">
				<div class="form-element-wrap">
					<label for="pledge-amount"><?php echo wp_kses_post( __( 'Amount pledged per point', 'charitypress' ) ); ?></label>
					<input type="text" id="pledge-amount" class="amount validate[required[min[.01]]]" placeholder="<?php echo wp_kses_post( _x( 'Amount', 'form placeholder', 'charitypress' ) ); ?>">
					<br>
					<small class="input"><?php echo wp_kses_post( __( 'All donations can be made in dollars or cents (.04, .05, etc.)', 'charitypress' ) ); ?></small>
				</div>
			</div>
			<div class="form-group">
				<div class="form-element-wrap">
					<label for="pledge-anonymous"><?php echo wp_kses_post( __( 'Make me anonymous', 'charitypress' ) ); ?>
						<br>
					</label>
					<input id="pledge-anonymous" type="checkbox" class="anonymous">
				</div>
			</div>
			<div class="form-group">
				<div class="form-element-wrap">
					<label for="charity-team"><?php echo wp_kses_post( _x( 'Team', 'form label', 'charitypress' ) ); ?></label>
					<?php
					$terms = get_terms( Charity_Team_User_Taxonomy::TAXONOMY_NAME, array( 'hide_empty' => false ) );
					if ( ! empty( $terms ) ) {
						?><select class="team" name="charity-team">
						<option value="" selected="selected"><?php echo wp_kses_post( __( '-- No Team --', 'charitypress' ) ); ?></option><?php
						foreach ( $terms as $term ) {
							?>
							<option value="<?php echo esc_attr( $term->slug ); ?>">
								<?php echo esc_html( $term->name ); ?></option>

						<?php
						}
						?></select>
						<strong class="input"><?php echo wp_kses_post( __( 'Join a team or create your own from your profile page.', 'charitypress' ) ); ?></strong><?php
					} /* If there are no team terms, display a message. */ else {
						echo wp_kses_post( __( 'There are no teams available.', 'charitypress' ) );
					} ?>
				</div>
			</div>
			<div class="form-group">
				<div class="estimator">

				</div>
			</div>
			<div class="form-group">
				<div class="form-element-wrap">
					<label for="pledge-email"><?php echo wp_kses_post( _x( 'Email Address', 'form label', 'charitypress' ) ); ?></label>
					<input id="pledge-email" type="text" class="email validate[required,custom[email]]" placeholder="<?php echo wp_kses_post( _x( 'Email', 'form placeholder', 'charitypress' ) ); ?>">
				</div>
			</div>
			<div class="form-group">
				<div class="form-element-wrap">
					<label for="pledge-cc"><?php echo wp_kses_post( _x( 'Credit Card Number', 'form label', 'charitypress' ) ); ?></label>
					<input id="pledge-cc" type="text" autocomplete="off" size="20" data-stripe="number" class="cc-number validate[required[creditCard]]" placeholder="<?php echo wp_kses_post( _x( 'Credit Card Number', 'form placeholder', 'charitypress' ) ); ?>">
				</div>
			</div>
			<div class="form-group">
				<div class="form-element-wrap">
					<label for="pledge-cc-cvc"><?php echo wp_kses_post( _x( 'Credit Card CVC', 'form label', 'charitypress' ) ); ?></label>
					<input id="pledge-cc-cvc" type="text" data-stripe="cvc" autocomplete="off" class="validate[required[number]]" placeholder="<?php echo wp_kses_post( _x( 'Credit Card CVC', 'form placeholder', 'charitypress' ) ); ?>">
				</div>
			</div>
			<div class="form-group">
				<div class="form-element-wrap">
					<label for="pledge-cc-expiration-mm"><?php echo wp_kses_post( _x( 'Credit Card Expiration', 'form label', 'charitypress' ) ); ?></label>
					<input maxlength="<?php echo wp_kses_post( _x( '2', 'Maxlength for MM', 'charitypress' ) ); ?>" id="pledge-cc-expiration-mm" type="text" class="half validate[required,min[0],max[12]" autocomplete="off" size="2" data-stripe="exp-month" class="required" placeholder="<?php echo wp_kses_post( _x( 'MM', 'form placeholder', 'charitypress' ) ); ?>">
					<input maxlength="<?php echo wp_kses_post( _x( '4', 'Maxlength for YYYY', 'charitypress' ) ); ?>" type="text" class="half validate[required,min[<?php echo date( 'Y' ); ?>]" autocomplete="off" size="4" data-stripe="exp-year" class="required" placeholder="<?php echo wp_kses_post( _x( 'YYYY', 'form placeholder', 'charitypress' ) ); ?>">
				</div>

			</div>
			<input type="hidden" name="type" class="type" value="pledger">
			<input type="hidden" name="nonce" class="nonce" value="<?php echo wp_create_nonce( 'charitypress-form-nonce' ); ?>">
			<button type="submit"><?php echo wp_kses_post( _x( 'Pledge', 'form button', 'charitypress' ) ); ?></button>

			<?php

			do_action( 'charitypress_after_pledge_form' );
			?>
		</form>
		<?php
		return ob_get_clean();


	}

	public function individual_leaderboard() {


		return charitypress_get_individual_leaderboard();

	}

	public function subscribe_form() {
		ob_start();
		if ( $logged_in_message = $this->is_user_logged_in() ) :
			ob_end_clean();

			return $logged_in_message;
		endif;
		?>

		<form class="charitypress-subscribe-form">
			<div class="status-message message"></div>
			<p><?php echo __( wp_kses_post( apply_filters( 'charitypress_subscribe_text', 'Subscribe for email updates' ) ), 'charitypress' ); ?></p>
			<input type="email" name="charitypress-subscribe-email-address" id="charitypress-subscribe-email-address">
			<button id="charitypress-subscribe-button"><?php echo __( wp_kses_post( apply_filters( 'charitypress_subscribe_button', 'Subscribe' ) ), 'charitypress' ); ?></button>
			<input type="hidden" name="nonce" class="nonce" value="<?php echo wp_create_nonce( 'charitypress-form-nonce' ); ?>">
		</form>
		<?php
		return ob_get_clean();

	}

	public function team_leaderboard() {

		return charitypress_get_team_leaderboard();

	}


}

$cs = new Charity_Shortcodes();
$cs->init();