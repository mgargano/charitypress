<?php

class Charity_Charge_Screen {

	const NONCE_GET_DATA = 'check-charge-user-nonce';
	const LAST_UPDATED_OPTION = 'charitypress_last_updated';

	public function init() {

		$this->attach_hooks();

	}

	public function attach_hooks() {

		add_action( 'admin_menu', array( $this, 'add_menu_item' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'admin_enqueue' ) );
	}

	function add_menu_item() {

		$page_title = 'Process Donations';
		$menu_title = 'Charge Users';
		$capability = 'manage_options';
		$menu_slug  = 'charge-users';
		$function   = array( $this, 'output_screen' );
		add_management_page( $page_title, $menu_title, $capability, $menu_slug, $function );

	}

	function output_screen() {
		$last_updated        = $this->get_last_updated();
		$seconds_offset      = get_option( 'gmt_offset' ) * 3600;
		$last_updated_offset = $last_updated + $seconds_offset;
		$last_updated_output = 'Not yet ran';
		if ( $last_updated_offset ) {
			$last_updated_output = date( "F j, Y, g:i a", $last_updated_offset );
		}

		?>
		<div id="message" class="updated fade" style="display:none"></div>

		<div class="wrap charge-users">
			<h2><?php echo _x( wp_kses_post( 'Charge Users' ), 'charge users screen', 'charitypress' ); ?></h2>

			<p>
				<label for="datePickerBegin"><?php echo _x( wp_kses_post( 'Start Date' ), 'charge users screen', 'charitypress' ); ?></label><input name="datePickerBegin" type="text" id="datePickerBegin">
			</p>

			<p>
				<label for="nounsInPeriod"><?php echo _x( wp_kses_post( ucwords( Charity_Settings::raw_get( 'plural_noun', '{{NOUN}' ) ) . ' ' . Charity_Settings::raw_get( 'past_tense_verb', '{{VERB}' ) . ' in Period' ), 'charge users screen', 'charitypress' ); ?></label><input name="nounsInPeriod" type="text" id="nounsInPeriod">
			</p>

			<p class="cf">
				<?php
				$qs = new Charity_Queue_Status;
				if ($qs->backlog_exists()) {
				$charge_queue = $qs->get_charge_queue();
				$email_queue  = $qs->get_email_queue();
				if (count( $charge_queue )) {
				?>

				<h2>Charges Queued</h2>
				<table class="queue">
					<tr>
						<td>User</td>
						<td>Charge Amount</td>
						<td>Edit User Link</td>
					</tr>
					<?php foreach ( $charge_queue as $charge ) { ?>
						<tr><?php echo sprintf( '<td>%s</td><td>%s</td><td><a target="_BLANK" href="%s">[Edit User]</a></td>', $charge['user']->display_name, $charge['charge_amount'], get_edit_user_link( $charge['user']->ID ) ); ?></tr><?php
					}
					?></table><?php
				}
				if ( count( $email_queue ) ) {
					?>
					<h2>Emails Queued</h2>
					<table class="queue">
					<tr>
						<td>To</td>
						<td>Subject</td>
						<td>Body</td>
						<td>Edit User Link</td>
					</tr>
					<?php
					foreach ( $email_queue as $email ) {
						?>
						<tr><?php echo sprintf( '<td>%s</td><td>%s</td><td>%s</td><td><a target="_BLANK" href="%s">[Edit User]</a></td>', $email['to'], $email['subject'], $email['body'], get_edit_user_link( $email['body'] ) ); ?></tr><?php
					}
					?></table>
					<?php

				}
			?><input class="button-primary" type="submit" id="reprocess-queue" value="<?php echo _x( wp_kses_post( 'Reprocess Queue' ), 'charge users screen', 'charitypress' ); ?>"><?php
			} else {
				?>
				<input class="button-primary" type="submit" id="process-donations" value="<?php echo _x( wp_kses_post( 'Process Donations' ), 'charge users screen', 'charitypress' ); ?>">
				<?php
			}

			?>
			<span class="spinner"></span><div class="cf"></div><br><div class="safety-wrap"><span class="unsafe">Do not close this window!</span><span class="safe">You are now safe to close this window.</span></div>
			</p>

			<div>
				<strong><?php echo _x( wp_kses_post( 'Last successfully ran:' ), 'charge users screen', 'charitypress' ); ?>
					<span id="last-updated" data-last-updated="<?php echo $last_updated_offset ? esc_attr( $last_updated_offset ) : esc_attr( 0 ); ?>"><?php echo $last_updated_output; ?></span></strong>
			</div>

			<h3><?php echo _x( wp_kses_post( 'Response' ), 'charge users screen', 'charitypress' ); ?></h3>
			<input type="hidden" id="last-ran-timestamp" value="<?php echo esc_attr( $last_updated ); ?>">

			<div class="response"></div>
		</div>

	<?php

	}

	public function get_last_updated() {

		$last_updated = get_option( self::LAST_UPDATED_OPTION, null );

		return $last_updated;

	}

	public function admin_enqueue( $hook ) {
		if ( strpos( $hook, 'charge-users' ) !== false ) {
			wp_enqueue_script( 'charge-users', plugins_url( '/js/admin/processDonations.js', dirname( __FILE__ ) ), array(
				'jquery',
				'jquery-ui-datepicker',
				'underscore'
			) );
			wp_localize_script( 'charge-users', 'chargeUsers', array(
				'nonce_get_data' => wp_create_nonce( self::NONCE_GET_DATA ),
				'length'         => Charity_Settings::raw_get( 'period_length', 'weekly' )
			) );

			wp_enqueue_style( 'process-donations', plugins_url( '/css/admin/processDonations.css', dirname( __FILE__ ) ) );
			wp_enqueue_style( 'process-donations-jquery-ui', plugins_url( '/css/admin/jquery-ui-1.10.4.custom.min.css', dirname( __FILE__ ) ) );

		}

	}


}

$ccs = new Charity_Charge_Screen;
$ccs->init();