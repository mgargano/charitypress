<?php

class Charity_Queue_Status {

	public function init() {
		$this->attach_hooks();
	}

	public function attach_hooks() {

		add_action( 'init', array( $this, 'add_notice' ) );

	}

	public function backlog_exists() {

		$email_queue  = $this->get_email_queue();
		$charge_queue = $this->get_charge_queue();
		$queues       = array_merge( $email_queue, $charge_queue );

		return ( count( $queues ) > 0 );

	}

	public function get_email_queue() {
		return get_option( Charity_Email_Queue::OPTION_NAME, array() );
	}

	public function get_charge_queue() {
		return get_option( Charity_Charge_Queue::OPTION_NAME, array() );
	}

	public function fix_array_keys(){

		$email_queue  = array_values( $this->get_email_queue() );
		$charge_queue = array_values( $this->get_charge_queue() );
		update_option( Charity_Charge_Queue::OPTION_NAME, $charge_queue );
		update_option( Charity_Email_Queue::OPTION_NAME, $email_queue );

	}

	public function add_notice() {

		if ( $this->backlog_exists() ) {
			Charity_Alerts::getInstance()->add_error( sprintf( 'There are charges and/or emails queued up. If you or another administrator is actively running the charge functionality you can safely ignore this. Otherwise if you inadvertently closed during the charging process or are not running the charging process, before you can make new charges you need to clear these up <a href="%s">here</a>.', admin_url( 'tools.php?page=charge-users' ) ) );
		}

	}


}

$qs = new Charity_Queue_Status;
$qs->init();