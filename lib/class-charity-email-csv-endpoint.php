<?php

class Charity_Email_CSV_Endpoint {


	public static function init() {
		add_rewrite_endpoint( 'csv', EP_ROOT );
		add_filter( 'template_include', array( __CLASS__, 'template_include' ) );
		add_filter( 'request', array( __CLASS__, 'request' ) );
		add_action( 'admin_menu', array( __CLASS__, 'create_csv_menu' ) );
		add_action( 'admin_init', array( __CLASS__, 'redirect_csv' ) );
	}


	public static function template_include( $template ) {

		if ( get_query_var( 'csv' ) ) {
			if ( current_user_can( 'manage_options' ) ) {
				self::build_email_csv();
				die;

			}

			if ( ! $template = get_404_template() ) {

				$template = get_index_template();
			}


		}


		return $template;
	}

	public static function request( $vars ) {
		if ( isset( $vars['csv'] ) ) {
			$vars['csv'] = true;
		}

		return $vars;
	}

	public static function build_email_csv() {

		$date = date( 'Ymd' );
		header( "Content-type: text/csv" );
		header( "Content-Disposition: attachment; filename=$date.csv" );
		header( "Pragma: no-cache" );
		header( "Expires: 0" );
		$users    = get_users();
		$template = '"%s","%s","%s"' . "\n";

		if ( count( $users ) ) {


			foreach ( $users as $user ) {


				$name        = $user->data->display_name;
				$email       = $user->data->user_email;
				$roles_array = $user->roles;
				$role_output = '';
				foreach ( $roles_array as $role ) {
					if ( '' !== $role_output ) {
						$role_output .= ',';
					}
					$role_output .= $role;
				}

				echo sprintf( $template, $name, $email, $role_output );


			}

		}
		$cs = new Charity_Subscriber;
		if ( $subscribers = $cs->get() ) {

			foreach ( $subscribers as $subscriber ) {
				echo sprintf( $template, null, $subscriber, 'subscriber' );

			}

		}

		die;
	}


	public static function create_csv_menu() {
		add_users_page(
			'CSV',
			'CSV',
			'manage_options',
			'csv-endpoint',
			'__return_false'


		);
	}

	public static function redirect_csv() {

		if ( array_key_exists( 'page', $_GET) && 'csv-endpoint' === $_GET['page'] ) {
			wp_redirect( home_url() . '/?csv=true' );
		}
	}


}

Charity_Email_CSV_Endpoint::init();

