<?php

if ( ! class_exists( 'TitanFramework' ) ) {

	Charity_Alerts::getInstance()->add_error( 'Titan Framework should have been included; make sure Composer Install has been run properly for this plugin.' );

	return;
}

class Charity_Settings {

	const INSTANCE_NAME = 'charitypress_settings';
	const PAGE_NAME = 'CharityPress';
	const PAGE_PARENT = 'options-general.php';

	/**
	 * Set up the class
	 */
	public function init() {

		$this->create_options();
		$this->attach_hooks();

	}

	/**
	 * Attach actions
	 */
	public function attach_hooks() {

		add_action( 'admin_head', array( $this, 'check_for_high_level_settings' ), PHP_INT_MAX - 1 );
		add_action( 'admin_head', array( $this, 'check_for_payment_settings' ), PHP_INT_MAX - 1 );

	}


	/**
	 * Helper class that returns settings page urls
	 * @return array
	 */
	public function settings_pages() {

		return array(

			'high-level' => admin_url( self::PAGE_PARENT . '?page=' . strtolower( self::PAGE_PARENT ) . '-' . strtolower( self::PAGE_NAME ) . '&tab=high-level-options' ),
			'payments'   => admin_url( self::PAGE_PARENT . '?page=' . strtolower( self::PAGE_PARENT ) . '-' . strtolower( self::PAGE_NAME ) . '&tab=payments' )

		);

	}

	/**
	 * Create options pages for CharityPress
	 */
	public function create_options() {

		$charitypress_settings = TitanFramework::getInstance( self::INSTANCE_NAME );
		$parent_page           = $charitypress_settings->createAdminPanel( array(
			'name'   => self::PAGE_NAME,
			'parent' => self::PAGE_PARENT
		) );
		$high_level            = $parent_page->createTab( array(
			'name' => 'High Level Options',
		) );

		$high_level->createOption(
			array(
				'name' => 'Singular Noun',
				'id'   => 'singular_noun',
				'type' => 'text',
				'desc' => 'e.g. point'
			)
		);
		$high_level->createOption(
			array(
				'name' => 'Plural Noun',
				'id'   => 'plural_noun',
				'type' => 'text',
				'desc' => 'e.g. points'
			)
		);
		$high_level->createOption(
			array(
				'name' => 'Present Tense Verb',
				'id'   => 'present_tense_verb',
				'type' => 'text',
				'desc' => 'e.g. score'
			)
		);

		$high_level->createOption(
			array(
				'name' => 'Past Tense Verb',
				'id'   => 'past_tense_verb',
				'type' => 'text',
				'desc' => 'e.g. scored'
			)
		);
		$high_level->createOption(
			array(
				'name' => 'Period Length',
				'id'   => 'period_length',
				'type' => 'select',
				'options' => array(
					'weekly' => 'Weekly',
					'monthly' => 'Monthly'
				),
				'default' => 'weekly',
			)
		);
		$high_level->createOption( array(
			'name' => 'Allow captains to see total pledged/donated',
			'id'   => 'allow_captains_to_see_totals',
			'type' => 'checkbox',
			'desc' => 'This is with regards to the "My Team" menu that donors and pledgers see (not visible to admins)'
		) );
		$high_level->createOption( array(
			'type' => 'save'
		) );
		$payments = $parent_page->createTab( array(
			'name' => 'Payments',
		) );

		$payments->createOption( array(
			'name' => 'Test Secret Key',
			'id'   => 'test_secret_key',
			'type' => 'text'
		) );

		$payments->createOption( array(
			'name' => 'Test Publishable Key',
			'id'   => 'test_publishable_key',
			'type' => 'text'
		) );

		$payments->createOption( array(
			'name' => 'Live Secret Key',
			'id'   => 'live_secret_key',
			'type' => 'text'
		) );

		$payments->createOption( array(
			'name' => 'Live Publishable Key',
			'id'   => 'live_publishable_key',
			'type' => 'text'
		) );

		$payments->createOption( array(
			'name'    => 'Environment',
			'id'      => 'environment',
			'type'    => 'select',
			'options' => array(
				'test'       => 'Staging (Test)',
				'production' => 'Production',
			),
			'default' => 'test',
		) );

		$payments->createOption( array(
			'name' => 'Message to display on donors and pledgers credit card statements',
			'id'   => 'charge_text',
			'type' => 'text'
		) );

		$payments->createOption( array(
			'type' => 'save'
		) );


		$email_templates = $parent_page->createTab( array(
			'name' => 'Email Templates/Options',
		) );

		$email_templates->createOption( array(
			'name' => 'Send copy of all emails to admins',
			'id'   => 'email_admins_user_emails',
			'type' => 'checkbox',
			'desc' => 'The email address that will receive these emails is the one in <strong>Settings</strong> > <strong>General</strong> > <strong>E-mail Address</strong>'
		) );

		$email_templates->createOption( array(
			'name' => 'Subject for email sent to user after they initially pledge',
			'id'   => 'email_subject_after_initial_pledge',
			'type' => 'text'
		) );

		$email_templates->createOption( array(
			'name' => 'Body for email sent to user after they initially pledge',
			'id'   => 'email_body_after_initial_pledge',
			'type' => 'editor',
			'desc' => "<strong>Template Tags</strong>: <br>{login} for login <br> {password} for password <br> {name} For the user's name <br> {login_url} for login URL <br> {site_name} for the site name<br>{amount_pledged} for the amount the user pledged<br>{singular_noun} for the singular noun set in high level settings<br>{plural_noun} for the plural noun set in high level settings<br>{present_tense_verb} for the present tense verb set in high level settings<br>{past_tense_verb} for the present tense verb set in high level settings"
		) );


		$email_templates->createOption( array(
			'name' => 'Subject for email sent to user after periodic (e.g. weekly) pledge',
			'id'   => 'email_subject_after_periodic_pledge',
			'type' => 'text'
		) );

		$email_templates->createOption( array(
			'name' => 'Body for email sent to user after periodic (e.g. weekly) pledge',
			'id'   => 'email_body_after_periodic_pledge',
			'type' => 'editor',
			'desc' => "<strong>Template Tags</strong>: <br>{name} For the user's name<br>{login_url} for login URL<br>{site_name} for the site name <br>{amount_charged} for the amount charged in this period <br>{nouns_in_period} for " . strtolower( self::raw_get( 'plural_noun', '{noun}' ) ) . " " . strtolower( self::raw_get( 'past_tense_verb', '{verb}' ) ) . " in period<br>{site_url} for the site url<br>{total_pledged} total user has charged since inception<br>{team_info} for Team Information<br>{campaign_total} Overall Total Pledged"
		) );


		$email_templates->createOption( array(
			'name' => 'Subject for email sent to user after direct donation',
			'id'   => 'email_subject_after_direct_donation',
			'type' => 'text'
		) );

		$email_templates->createOption( array(
			'name' => 'Body for email sent to user after direct donation',
			'id'   => 'email_body_after_direct_donation',
			'type' => 'editor',
			'desc' => "<strong>Template Tags</strong>: <br>{login} for login <br> {password} for password <br> {name} For the user's name <br> {login_url} for login URL <br> {site_name} for the site name<br>{amount_pledged} for the amount the user pledged<br>{singular_noun} for the singular noun set in high level settings<br>{plural_noun} for the plural noun set in high level settings<br>{present_tense_verb} for the present tense verb set in high level settings<br>{past_tense_verb} for the present tense verb set in high level settings"
		) );

		$email_templates->createOption( array(
			'name' => 'Subject for email sent to user after they subscribe',
			'id'   => 'email_subject_after_user_subscribes',
			'type' => 'text'
		) );

		$email_templates->createOption( array(
			'name' => 'Body for email sent to user after they subscribe',
			'id'   => 'email_body_after_user_subscribes',
			'type' => 'editor'
		) );

		$email_templates->createOption( array(
			'name' => 'Team Info (fill this out and it will be the template tag for {team_info} in the above emails. If you do not fill this out, {team_info} will be blank in the emails.',
			'id'   => 'team_info',
			'type' => 'editor',
			'desc' => "<strong>Template Tags</strong>: <br>{team_name} for team name <br> {team_amount} for total donated for the team <br> {team_number_of_members} for the number of team members"
		) );

		$email_templates->createOption( array(
			'type' => 'save'
		) );

	}

	/**
	 * Helper method for retrieving options
	 *
	 * @param      $id
	 * @param null $default
	 *
	 * @return mixed|null|string
	 */
	public static function get( $id, $default = null ) {

		$titan = TitanFramework::getInstance( self::INSTANCE_NAME );
		$value = $titan->getOption( $id );
		if ( ! $value ) {
			$value = $default;
		}

		return $value;

	}

	/**
	 * Check high level settings, output notices if required fields aren't set
	 */
	public function check_for_high_level_settings() {
		$singular_noun      = static::get( 'singular_noun' );
		$present_tense_verb = static::get( 'present_tense_verb' );
		$plural_noun        = static::get( 'plural_noun' );
		$past_tense_verb    = static::get( 'past_tense_verb' );
		if ( ! $singular_noun || ! $present_tense_verb || ! $plural_noun || ! $past_tense_verb ) {
			$settings_pages = $this->settings_pages();
			$settings_url   = $settings_pages['high-level'];
			Charity_Alerts::getInstance()->add_error( wp_kses_post( __( 'Please add your CharityPress <strong>Nouns</strong> and <strong>Verbs</strong> in the <a href="' . esc_url( $settings_url ) . '">CharityPress high level settings page</a>.', 'charitypress' ) ) );
		}


	}

	/**
	 * Check payment settings, output notices if required fields aren't set
	 */
	public function check_for_payment_settings() {
		$environment = static::get( 'environment' );

		$publishable_key = $secret_key = false;

		if ( 'test' === $environment ) {

			$publishable_key = static::get( 'test_publishable_key' );
			$secret_key      = static::get( 'test_secret_key' );


		} elseif ( 'production' === $environment ) {

			$publishable_key = static::get( 'live_publishable_key' );
			$secret_key      = static::get( 'live_secret_key' );

		}


		if ( ! $publishable_key || ! $secret_key ) {
			$settings_pages = $this->settings_pages();
			$settings_url   = $settings_pages['payments'];
			Charity_Alerts::getInstance()->add_error( __( 'You are missing API keys for your current environment (<strong>' . __( $environment, 'charitypress' ) . '</strong>). Please add add them in the <a href="' . esc_url( $settings_url ) . '">CharityPress payments settings page</a>.', 'charitypress' ) );
		}


	}

	public static function raw_get( $key, $default ) {
		$option = unserialize( get_option( self::INSTANCE_NAME . '_options' ) );

		return isset( $option[ $key ] ) ? $option[ $key ] : $default;
	}


}

$charity_settings = new Charity_Settings;
$charity_settings->init();