<?php

class Charity_User {

	private $status = array();
	private $success = false;
	private $user = null;

	public function create( $data = array() ) {

		$post_logger = new Charity_Post_Logger;

		$defaults = array(
			'first_name'  => null,
			'last_name'   => null,
			'email'       => null,
			'amount'      => null,
			'token'       => null,
			'team'        => null,
			'anonymous'   => null,
			'customer_id' => null,
			'type'        => null
		);
		$data     = array_merge( $defaults, $data );

		if ( ! $data['first_name'] ) {
			$this->success  = false;
			$this->status[] = _x( 'Missing First Name.', 'user creation error', 'charitypress' );
		}

		if ( ! $data['type'] ) {
			$this->success  = false;
			$this->status[] = _x( 'Missing User Type.', 'user creation error', 'charitypress' );
		}
		if ( ! $data['last_name'] ) {
			$this->success  = false;
			$this->status[] = _x( 'Missing Last Name.', 'user creation error', 'charitypress' );
		}
		if ( ! is_email( $data['email'] ) ) {
			$this->success  = false;
			$this->status[] = _x( 'Invalid Email Address.', 'user creation error', 'charitypress' );
		}
		if ( email_exists( $data['email'] ) ) {
			$this->success  = false;
			$this->status[] = _x( 'Email already exists.', 'user creation error', 'charitypress' );
		}
		if ( ! $data['amount'] || ! is_numeric( $data['amount'] ) ) {
			$this->success  = false;
			$this->status[] = _x( 'Missing Amount.', 'user creation error', 'charitypress' );
		}
		if ( ! $data['token'] ) {
			$this->success  = false;
			$this->status[] = _x( 'Invalid Token from Stripe.', 'user creation error', 'charitypress' );
		}

		if ( ! $data['customer_id'] ) {
			$this->success  = false;
			$this->status[] = _x( 'Invalid Customer ID from Stripe.', 'user creation error', 'charitypress' );
		}

		if ( count( $this->status ) > 0 ) {
			return false; // we have an error of some sort
		}


		$name             = wp_kses_post( $data['first_name'] . '  ' . $data['last_name'] );
		$password         = wp_generate_password(); // let's generate a password since we are hijacking wordpress' new user email
		$data['password'] = $password;
		$user_data        = array(
			'user_login'    => sanitize_email( $data['email'] ),
			'user_nicename' => $name, //escaped above using wp_kses_post
			'user_email'    => sanitize_email( $data['email'] ),
			'role'          => $data['type'],
			'user_pass'     => $password,
			'nickname'      => $name, //escaped above using wp_kses_post
			'first_name'    => wp_kses_post( $data['first_name'] ),
			'last_name'     => wp_kses_post( $data['last_name'] )
		);

		$this->user      = wp_insert_user( $user_data );
		$data['user_id'] = $this->user;

		if ( is_wp_error( $this->user ) ) {
			$this->status[] = $this->user->get_error_message();
			$this->success  = false;

			return false; // we have an error of some sort
		}

		$user_object = get_user_by( 'id', $this->user );
		$user_logger = new Charity_User_Logger( $user_object );
		$post_logger = new Charity_Post_Logger();
		$notice      = sprintf( '%s - Created user %s %s (%s) as %s <a href="%s">[Edit User]</a>', date( 'm/d/Y  H:i:s', time() + get_option( 'gmt_offset' ) * 3600 ), $data['first_name'], $data['last_name'], $data['email'], $data['type'], get_edit_user_link( $this->user ) );
		$post_logger->log_notice( $notice, $notice );
		$user_logger->log( $notice );


		if ( $data['team'] && $team_object = $this->sanitize_team_slug( $data['team'] ) ) {

			wp_set_object_terms( $this->user, array( $team_object->slug ), Charity_Team_User_Taxonomy::TAXONOMY_NAME, false );


		}
		$this->set_user_active( $this->user );
		update_user_meta( $this->user, 'customer-token', (string) $data['customer_id'] );
		if ( Charity_Role_Manager::ONE_TIME_ROLE === $data['type'] ) {
			//@todo rewrite this a little more OO will ya?
			$charge_text = Charity_Settings::get( 'charge_text' );
			if ( ! trim( $charge_text ) ) {
				$charge_text = html_entity_decode( get_bloginfo( 'name' ) );
			}
			$user_roles = '';
			if ( ! empty( $user_object->roles ) && is_array( $user_object->roles ) ) {
				foreach ( $user_object->roles as $role ) {
					if ( $user_roles ) {
						$user_roles .= ', ';
					}
					$user_roles .= $role;
				}
			}
			$log_note    = sprintf( '%s <br>Email: %s<br>Name: %s<br>Amount: %s<br> User ID: %s<br>Customer token: %s<br>Role(s): %s<br><a target="_BLANK" href="%s">Edit User</a>', current_time( 'mysql' ), $user_object->data->user_email, $user_object->data->display_name, number_format( (float) $data['amount'], 2 ), $user_object->ID, $data['token'], $user_roles, get_edit_user_link( $this->user ) );
			$log_summary = sprintf( 'Charged user: %s, %s <a href="%s">[Edit User]</a>', $user_object->display_name, number_format( (float) $data['amount'], 2 ), get_edit_user_link( $user_object->ID ) );

			$args                   = array();
			$args['charge_amount']  = (float) $data['amount'];
			$args['charge_text']    = $charge_text;
			$args['customer_token'] = (string) $data['customer_id'];
			$args['log_note']       = $log_note;
			$args['log_summary']    = $log_summary;
			$args['user_id']        = $this->user;
			$charge                 = Charity_Stripe::charge( $args );
			if ( $charge->success ) {
				update_user_meta( $this->user, 'total-donated', (float) $data['amount'] );
			}
		} elseif ( Charity_Role_Manager::RECURRING_ROLE === $data['type'] ) {
			update_user_meta( $this->user, 'pledge-per-noun', (float) $data['amount'] );
		}
		if ( 'true' === $data['anonymous'] ) {  //its set to a string
			update_user_meta( $this->user, 'anonymous', 'on' ); // set it to 'on' which is what we are using in the profile field
		}
		$data          = array_merge( $data, array( 'password' => $password ) );
		$charity_email = new Charity_Email();

		$data['post_logger'] = $post_logger;
		$data['user_logger'] = $user_logger;

		$charity_email->send_new_user_email( $data );
		$email_result = $charity_email->status();

		if ( ! $email_result->success ) {


			$post_logger->log_error( 'Error sending emails, do you have an email set up properly? <a href="http://codex.wordpress.org/Function_Reference/wp_mail" target="_BLANK">See this</a>', 'Email issues' );

		}

		$this->success  = true;
		$message        = sprintf( _x( 'Thank you for your support! Your username and password have been sent to <strong>%s</strong>. Feel free to update your profile and/or set your password below.', 'thank you for pledging', 'charitypress' ), sanitize_email( $data['email'] ) );
		$this->status[] = __( $message, 'charitypress' );
		Charity_User_Alerts::getInstance()->add_notice( $message, $this->user );

		wp_set_auth_cookie( $this->user, true );

		return true;


	}

	public function result() {

		$post_logger = new Charity_Post_Logger;

		$result          = new stdClass();
		$result->success = $this->success;
		$result->status  = $this->status;
		if ( ! $result->success && count( $result->status ) === 0 ) {
			$result->status = array( _x( 'Generic Error.', 'user creation error', 'charitypress' ) );
		}
		if ( ! $result->status ) {
			$post_logger->log_error( $this->status );
		}

		return $result;

	}

	public function sanitize_team_slug( $slug ) {

		$term = get_term_by( 'slug', $slug, Charity_Team_User_Taxonomy::TAXONOMY_NAME );

		return $term;

	}

	public function set_user_active( $user_id ) {

		update_user_meta( $user_id, 'charitypress-active', true );

	}

	public function set_user_inactive( $user_id ) {

		update_user_meta( $user_id, 'charitypress-active', false );

	}

	public function user_active( $user_id ) {

		return get_user_meta( $user_id, 'charitypress-active', true );

	}


}