<?php

class Charity_User_Logger {

	private $user;
	const USER_META_KEY = 'charitypress_log';

	public function __construct( WP_User $user ) {

		$this->user = $user;

	}

	public function log( $message ) {


		$user_log = (string) get_user_meta( $this->user->ID, self::USER_META_KEY, true );

		if ( $user_log ) {
			$user_log .= '<br>';
		}

		$user_log .= $message;


		update_user_meta( $this->user->ID, self::USER_META_KEY, $user_log );

	}


}