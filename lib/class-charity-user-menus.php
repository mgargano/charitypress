<?php

class Charity_User_Menus {

	public function init() {

		add_action( 'admin_menu', array( $this, 'add_menus' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'admin_enqueue_scripts' ) );

	}

	function admin_enqueue_scripts() {

		?>
		<style type="text/css">
			.donor-tables tr,
			{
				text-align: left;
				border:     1px solid #000;

			}

			.donor-tables td,
			.donor-tables th {
				padding:       7px 10px;
				border-bottom: 1px solid #666;
				border-right:  1px solid #666;
				text-align:    left;
			}

			.donor-tables th {
				border-top: 1px solid #666;
			}

			.donor-tables tbody tr:nth-child(even) {
				background: #FFF;
			}

			.donor-tables thead tr {
				background: #FFF;
			}

			.donor-tables th:first-child,
			.donor-tables td:first-child {
				border-left: 1px solid #666;

			}
		</style>
	<?php


	}

	public function add_menus() {

		add_submenu_page( 'users.php', 'Team Distribution', 'Team Distribution', 'manage_options', 'team-distribution', array(
			$this,
			'output_screen_distribution'
		) );
		add_submenu_page( 'users.php', 'User Contributions', 'User Contributions', 'manage_options', 'user-donations', array(
			$this,
			'output_screen_user_donations'
		) );


	}

	public function output_screen_distribution() {

		?>

		<div class="wrap">
			<h2>Team Distribution</h2>

			<?php $this->get_user_teams(); ?>

		</div>

	<?php


	}

	public function output_screen_user_donations() {

		?>

		<div class="wrap">
			<h2>User Contributions</h2>
			<?php $this->get_user_donations(); ?>
		</div>

	<?php


	}

	public function get_user_donations() {

		$users = array_merge(
			get_users(
				array( 'role' => Charity_Role_Manager::RECURRING_ROLE )
			),
			get_users(
				array(
					'role' => Charity_Role_Manager::ONE_TIME_ROLE
				)
			)
		);

		if ( count( $users ) === 0 ) {
			echo 'No donors yet!';

			return;
		}

		foreach ( $users as $user ) {

			$user_name                = $user->first_name . ' ' . $user->last_name;
			$user_id                  = $user->ID;
			$amount                   = get_user_meta( $user_id, 'total-donated', true );
			$amount_pledged_per_point = get_user_meta( $user_id, 'pledge-per-noun', true );


			$user_array[ $user->last_name ] = array(
				'name'                     => $user_name,
				'amount_pledged_per_point' => $amount_pledged_per_point,
				'amount'                   => $amount,
				'link'                     => get_edit_user_link( $user_id ),
				'user'                     => $user
			);

		}
		krsort( $user_array );
		?>
		<table border="0" cellpadding="0" cellspacing="0" class="donor-tables">
		<thead>
		<tr>
			<th>User</th>
			<th>Donated</th>
			<th>Amount Pledged Per Point</th>
			<th>Edit Link</th>
		</tr>
		</thead>
		<tbody>
		<?php
		foreach ( $user_array as $user_last_name => $user ) {

			?>
			<tr>

				<td><?php echo $user['name'] ?></td>
				<td>$<?php echo number_format( $user['amount'], 2, ".", "," ); ?></td>
				<?php if ( is_array( $user['user']->roles ) && in_array( Charity_Role_Manager::RECURRING_ROLE, $user['user']->roles ) ) : ?>
					<td>$<?php echo number_format( (double)$user['amount_pledged_per_point'], 2, ".", "," ); ?></td>
				<?php else : ?>
					<td>N/A</td>
				<?php endif; ?>
				<td><a href="<?php echo $user['link']; ?>">[edit user]</a></td>
			</tr> <?php
		}
		?></tbody></table><?php
	}

	public function get_user_teams() {

		$users = array_merge(
			get_users(
				array( 'role' => Charity_Role_Manager::RECURRING_ROLE )
			),
			get_users(
				array(
					'role' => Charity_Role_Manager::ONE_TIME_ROLE
				)
			)
		);
		$teams = array();

		if ( count( $users ) === 0 ) {
			echo 'No pledgers or donors yet!';

			return;
		}

		$team_array = $user_array = array();

		foreach ( $users as $user ) {


			$user_id   = $user->ID;
			$user_name = $user->first_name . ' ' . $user->last_name;
			if ( ! trim( $user_name ) ) {
				$user_name = $user->user_nicename;
			}
			$user_array[ $user_id ] = array( 'name' => $user_name, 'link' => get_edit_user_link( $user_id ) );

			$team = wp_get_object_terms( $user_id, 'team' );

			if ( count( $team ) ) {
				$team_array[ $team[0]->term_id ] = $team[0]->name;
				$teams[ $team[0]->term_id ][] = $user_id;
			}

		}
		if ( count( $teams ) > 0 ) {
			foreach ( $teams as $team_id => $user_ids ) {


				?><h2><?php echo $team_array[ $team_id ]; ?></h2><?php
				foreach ( $user_ids as $user_id ) {


					?><p><?php echo $user_array[ $user_id ]['name'] ?>
					<a href="<?php echo $user_array[ $user_id ]['link']; ?>">[edit user]</a></p><?php

				}

			}
		}

	}


}

$cm = new Charity_User_Menus;
$cm->init();