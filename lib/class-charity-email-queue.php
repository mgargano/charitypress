<?php

class Charity_Email_Queue {

	const OPTION_NAME = 'charitypress_email_queue';

	public static function add( $to, $subject, $body, WP_User $user ) {

		$option   = get_option( self::OPTION_NAME, array() );
		$option[] = array( 'to' => $to, 'subject' => $subject, 'body' => $body, 'id' => $user->ID );

		return update_option( self::OPTION_NAME, $option );

	}

	public static function get() {
		return get_option( self::OPTION_NAME );
	}


	public static function set_html_content_type() {
		return 'text/html';
	}

	public static function delete( $index ) {

		$option = get_option( self::OPTION_NAME, array() );
		unset( $option[ $index ] );

		return update_option( self::OPTION_NAME, $option );

	}


}