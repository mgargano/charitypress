<?php

class Charity_AJAX {

	public function init() {

		$this->attach_hooks();

	}

	public function attach_hooks() {

		add_action( 'wp_ajax_nopriv_charitypress_initialize_user', array( $this, 'charitypress_initialize_user' ) );
		add_action( 'wp_ajax_charitypress_build_charges', array( $this, 'charitypress_build_charges' ) );
		add_action( 'wp_ajax_charitypress_rebuild_totals', array( $this, 'charitypress_rebuild_totals' ) );
		add_action( 'wp_ajax_charitypress_charge_user_periodic', array( $this, 'charitypress_charge_user_periodic' ) );
		add_action( 'wp_ajax_charitypress_set_last_updated', array( $this, 'charitypress_set_last_updated' ) );
		add_action( 'wp_ajax_charitypress_send_periodic_emails', array( $this, 'charitypress_send_periodic_emails' ) );
		add_action( 'wp_ajax_charitypress_get_periodic_emails', array( $this, 'charitypress_get_periodic_emails' ) );
		add_action( 'wp_ajax_charitypress_send_email_periodic', array( $this, 'charitypress_send_email_periodic' ) );
		add_action( 'wp_ajax_charitypress_reprocess_queue', array( $this, 'charitypress_reprocess_queue' ) );
		add_action( 'wp_ajax_charitypress_fix_queue', array( $this, 'charitypress_fix_queue' ) );
		add_action( 'wp_ajax_charitypress_suggest_team', array( $this, 'charitypress_suggest_team' ) );
		add_action( 'wp_ajax_nopriv_charitypress_add_subscriber', array( $this, 'charitypress_add_subscriber' ) );
		add_action( 'wp_ajax_charitypress_add_subscriber', array( $this, 'charitypress_add_subscriber' ) );
		add_action( 'wp_ajax_charitypress_suggested_team_action', array(
			$this,
			'charitypress_suggested_team_action'
		) );
	}

	public function charitypress_initialize_user() {

		check_ajax_referer( 'charitypress-form-nonce', 'nonce' );
		$first_name = $_POST['firstName'];
		$last_name  = $_POST['lastName'];
		$email      = $_POST['email'];
		$amount     = $_POST['amount'];
		$token      = $_POST['token'];
		$team       = isset( $_POST['team'] ) ? $_POST['team'] : '';
		$anonymous  = $_POST['anonymous'];
		$type       = $_POST['type'];

		$data  = compact( 'first_name', 'last_name', 'email', 'amount', 'token', 'team', 'anonymous', 'type' );
		$crm   = new Charity_Role_Manager;
		$roles = array_keys( $crm->get_roles() );
		if ( ! in_array( $type, $roles ) ) {
			return array(
				'success' => false,
				'status'  => __( 'Invalid role supplied', 'charitypress' )
			);
		}

		try {
			$customer      = Stripe_Customer::create( array(
					"card"        => $token,
					"email"       => $email,
					'description' => $first_name . ' ' . $last_name . ' directly donates ' . $amount,
				)
			);
			$customer_id   = $customer->id;
			$customer_data = compact( 'customer_id' );

			$user = new Charity_User();
 			$user->create( array_merge( $customer_data, $data ) );
			$result = (object) array_merge( (array) $user->result(), array( 'url' => admin_url( 'profile.php' ) ) );
			wp_send_json( $result );


		} catch ( Exception $ex ) {
			$response = (object) array(
				'success' => false,
				'status'  => __( $ex->getMessage(), 'charitypress' )
			);

			wp_send_json( $response );
		}

	}

	public function charitypress_build_charges() {


		check_ajax_referer( Charity_Charge_Screen::NONCE_GET_DATA, 'security' );
		$users           = get_users( array( 'role' => Charity_Role_Manager::RECURRING_ROLE ) );
		$nouns_in_period = (int) $_POST['nounsInPeriod'];
		foreach ( $users as $user ) {

			$pledge_per_noun = (float) get_user_meta( $user->ID, 'pledge-per-noun', true );
			$charge_amount   = (float) ( $nouns_in_period * $pledge_per_noun );
			$cu              = new Charity_User;
			if ( ! $cu->user_active( $user->ID ) ) { //only charge active users!
				continue;
			}
			$customer_token = get_user_meta( $user->ID, 'customer-token', true );

			$charge_text = Charity_Settings::get( 'charge_text' );
			if ( ! trim( $charge_text ) ) {
				$charge_text = html_entity_decode( get_bloginfo( 'name' ) );
			}

			$user_roles = '';
			if ( ! empty( $user->roles ) && is_array( $user->roles ) ) {
				foreach ( $user->roles as $role ) {
					if ( $user_roles ) {
						$user_roles .= ', ';
					}
					$user_roles .= $role;
				}
			}

			$log_note    = sprintf( '%s <br>Email: %s<br>Name: %s<br>Amount: %s<br> User ID: %s<br>Customer token: %s<br>Role(s): %s<br><a target="_BLANK" href="%s">[Edit User]</a>', current_time( 'mysql' ), $user->data->user_email, $user->data->display_name, number_format( (float) $charge_amount / 100, 2 ), $user->ID, $customer_token, $user_roles, get_edit_user_link( $user->ID ) );
			$log_summary = sprintf( 'Charged user: %s, %s <a href="%s">[Edit User]</a>', $user->display_name, number_format( (float) $charge_amount, 2 ), get_edit_user_link( $user->ID ) );
			$args        = compact( 'charge_amount', 'customer_token', 'user', 'charge_text', 'log_note', 'log_summary' );
			if ( (float) $args['charge_amount'] > 0 ) {
				Charity_Charge_Queue::add( $args );
			}

		}
		wp_send_json( Charity_Charge_Queue::get() );


	}

	public function charitypress_charge_user_periodic() {

		check_ajax_referer( Charity_Charge_Screen::NONCE_GET_DATA, 'security' );

		$charge_amount  = (float) $_POST['charge_amount'];
		$charge_text    = wp_kses_post( $_POST['charge_text'] );
		$customer_token = wp_kses_post( $_POST['customer_token'] );
		$log_note       = wp_kses_post( $_POST['log_note'] );
		$log_summary    = wp_kses_post( $_POST['log_summary'] );
		$user_id        = absint( $_POST['user_id'] );
		$user_object    = get_user_by( 'id', $user_id );
		$force          = $_POST['force'] === 'true' ? true : false;


		$index           = (int) $_POST['index'];
		$nouns_in_period = absint( $_POST['nounsInPeriod'] );
		$date_begin      = (int) $_POST['date_begin'];
		$registered_date = strtotime( $user_object->data->user_registered );

		Charity_Charge_Queue::delete( $index );

		if ( $date_begin < $registered_date && ! $force ) {

			$response                = new stdClass();
			$response->statusMessage = sprintf( 'Skipped user %s they registered after this periods start date %s <a href="%s">[Edit User]</a>', $user_object->display_name, date( "F j, Y, g:i a", $date_begin + get_option( 'gmt_offset' ) ), get_edit_user_link( $user_object->ID ) );

			wp_send_json( $response );

		}

		$args     = compact( 'charge_amount', 'charge_text', 'customer_token', 'log_note', 'log_summary', 'user_id' );
		$response = Charity_Stripe::charge( $args );

		if ( $response->success ) {
			$total_donated_before_charge = (float) get_user_meta( $user_id, 'total-donated', true );
			$total_donated_after_charge  = (float) $total_donated_before_charge + $charge_amount;
			update_user_meta( $user_id, 'total-donated', $total_donated_after_charge );
			$ce = new Charity_Email;
			$ce->queue_periodic_charge_email( $user_object, array(
				'amount_charged'  => $charge_amount,
				'nouns_in_period' => $nouns_in_period,
			) );
		}

		wp_send_json( $response );

	}

	public function charitypress_set_last_updated() {

		check_ajax_referer( Charity_Charge_Screen::NONCE_GET_DATA, 'security' );
		$option = Charity_Charge_Screen::LAST_UPDATED_OPTION;
		update_option( $option, current_time( 'timestamp', 1 ) );
		wp_send_json_success();

	}

	public function charitypress_send_periodic_emails() {

		check_ajax_referer( Charity_Charge_Screen::NONCE_GET_DATA, 'security' );
		if ( Charity_Email_Queue::send() ) {
			wp_send_json_success( array( 'message' => 'Emails completed!' ) );
		} else {
			wp_send_json_error( array( 'message' => 'Either a failure in sending emails or there are not any registered users eligible to be charged, so no emails were sent.' ) );
		}

	}

	public function charitypress_get_periodic_emails() {
		check_ajax_referer( Charity_Charge_Screen::NONCE_GET_DATA, 'security' );
		wp_send_json( Charity_Email_Queue::get() );

	}

	public function charitypress_send_email_periodic() {
		check_ajax_referer( Charity_Charge_Screen::NONCE_GET_DATA, 'security' );
		$user_id  = absint( $_POST['user_id'] );
		$index    = (int) $_POST['index'];
		$to       = wp_kses_post( $_POST['to'] );
		$subject  = wp_kses_post( $_POST['subject'] );
		$body     = wp_kses_post( $_POST['body'] );
		$response = Charity_Email::wp_mail( $user_id, $to, $subject, $body );
		Charity_Email_Queue::delete( $index );
		wp_send_json( $response );
	}

	public function charitypress_reprocess_queue() {
		check_ajax_referer( Charity_Charge_Screen::NONCE_GET_DATA, 'security' );
		$cqs          = new Charity_Queue_Status;
		$charge_queue = array_values( (array) $cqs->get_charge_queue() );
		wp_send_json( $charge_queue );

	}

	public function charitypress_fix_queue() {
		check_ajax_referer( Charity_Charge_Screen::NONCE_GET_DATA, 'security' );
		$cqs = new Charity_Queue_Status;
		$cqs->fix_array_keys();
		wp_send_json_success();
	}

	public function charitypress_add_subscriber() {
		check_ajax_referer( 'charitypress-form-nonce', 'nonce' );
		$email = is_email( $_POST['email'] );
		if ( ! $email ) {
			wp_send_json_error( array( 'message' => 'Invalid email address.' ) );
		}
		$cs  = new Charity_Subscriber;
		$add = $cs->add( $email );
		if ( 'exists' === $add ) {
			wp_send_json_success( array( 'message' => 'Email address already subscribed.' ) );
		}
		if ( $cs->add( $email ) ) {
			wp_send_json_success();
		}


	}

	public function charitypress_rebuild_totals() {
		check_ajax_referer( Charity_Charge_Screen::NONCE_GET_DATA, 'security' );
		Charity_Totals::rebuild_totals();
		wp_send_json_success();
	}


	public function charitypress_suggest_team() {

		check_ajax_referer( Charity_Suggest_Team_Name::NONCE_NAME, 'security' );
		$suggested_team_name = sanitize_text_field( $_POST['suggested'] );
		$cstn                = new Charity_Suggest_Team_Name();
		$cstn->user_suggested( $suggested_team_name );


	}

	public function charitypress_suggested_team_action() {

		check_ajax_referer( Charity_Suggest_Team_Name::NONCE_NAME, 'security' );
		$types = array( 'accept', 'reject' );
		$type  = ( in_array( $_POST['type'], $types ) ) ? $_POST['type'] : null;
		$index = (int) $_POST['index'];
		$cstn  = new Charity_Suggest_Team_Name();
		$cstn->team_action( $type, $index );


	}

}

$charity_ajax = new Charity_AJAX;
$charity_ajax->init();