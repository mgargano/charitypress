<?php

class Charity_User_Team_Menu {


	public function init() {
		$this->attach_hooks();
	}

	public function attach_hooks() {
		add_action( 'admin_menu', array( $this, 'menu_item' ) );
	}


	public function menu_item() {
		$captains_only = false; //set to true if you want captains only

		if ( $captains_only && ! get_user_meta( get_current_user_id(), 'is-captain', true ) === 'on' ) {
			return;
		}

		if ( current_user_can( 'is_charitypress' ) && ! current_user_can( 'manage_options' ) ) {
			add_menu_page( 'My Team', 'My Team', 'is_charitypress', 'team-output', array(
				$this,
				'output_screen'
			), 'dashicons-groups' );
		}


	}


	public function output_screen() {

		$user_id = get_current_user_id();

		$team_linked_objects = wp_get_object_terms( get_current_user_id(), Charity_Team_User_Taxonomy::TAXONOMY_NAME );
		$team_object         = null;

		if ( is_array( $team_linked_objects ) && count( $team_linked_objects ) ) {
			$team_object = $team_linked_objects[0];
		}


		?>
		<div class="wrap"><?php
		if ( ! $team_object ) {
			?><h2>Team Stats</h2><br>
			You are not on a team! Select a team in your
			<a href="<?php echo get_edit_user_link( $user_id ); ?>">profile page</a>.<?php
			return;

		}
		?><h2>Team "<?php echo $team_object->name; ?>" Stats</h2>
		<br><br>

		<table border="0" cellpadding="0" cellspacing="0" class="donor-tables">
			<thead>
			<tr>
				<th>User</th>
				<?php if (get_user_meta( $user_id, 'is-captain', true ) && Charity_Settings::get( 'allow_captains_to_see_totals' )) { ?>
				<th>Donated to Date</th>
				<th>Amount Pledged Per <?php echo Charity_Settings::get( 'singular_noun', '{{NOUN}}' ); ?></th>
			</tr>
			<?php } ?>

			</thead>
			<tbody>


			<?php



			$terms = get_objects_in_term( $team_object->term_id, 'team' );
			foreach ( $terms as $user_id ) {

				$user = get_user_by( 'id', $user_id );

				if ( $user ) {

					$user_name               = $user->display_name;
					$amount                  = get_user_meta( $user->ID, 'total-donated', true );
					$amount                  = number_format( (float) $amount, 2 );
					$amount_pledged_per_noun = get_user_meta( $user->ID, 'pledge-per-noun', true );
					$amount_pledged_per_noun = number_format( (float) $amount_pledged_per_noun, 2 );

					?>
					<tr>
						<td><?php echo $user_name; ?></td>

						<?php if ( get_user_meta( get_current_user_id(), 'is-captain', true ) === 'on' && Charity_Settings::get( 'allow_captains_to_see_totals' ) ) { ?>
							<td>$<?php echo $amount; ?></td>
							<td>$<?php echo $amount_pledged_per_noun; ?></td>
						<?php } ?>

					</tr>
				<?php

				}

			}

			?>
			</tbody>
		</table>
	<?php


	}


}

$cutm = new Charity_User_Team_Menu();
$cutm->init();