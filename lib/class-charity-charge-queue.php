<?php

class Charity_Charge_Queue {

	const OPTION_NAME = 'charitypress_charge_queue';

	public static function add( $args ) {
		$option   = get_option( self::OPTION_NAME, array() );
		$option[] = $args;

		return update_option( self::OPTION_NAME, $option );

	}

	public static function get() {
		return get_option( self::OPTION_NAME );
	}

	public static function delete( $index ) {

		$option = get_option( self::OPTION_NAME, array() );
		unset( $option[ $index ] );

		return update_option( self::OPTION_NAME, $option );

	}

}

