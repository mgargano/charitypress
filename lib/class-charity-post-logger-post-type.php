<?php

class Charity_Post_Logger_Post_Type {

	const POST_TYPE = 'charitypress_log';
	const META_KEY = 'log_content';
	const TAXONOMY_NAME = 'log_type';

	public function init() {

		$this->attach_hooks();


	}

	public function attach_hooks() {

		add_action( 'init', array( $this, 'register_post_type' ), 1 );
		add_action( 'init', array( $this, 'register_taxonomy' ), 2 );
		add_action( 'add_meta_boxes', array( $this, 'add_metabox' ) );
		add_action( 'restrict_manage_posts', array( $this, 'filter_by_type' ) );


		add_filter( 'manage_edit-'. self::POST_TYPE . '_columns', array( $this, 'taxonomy_column' ) );
		add_action( 'manage_'. self::POST_TYPE . '_posts_custom_column', array( $this, 'taxonomy_column_content' ), 10, 2 );


	}

	public function add_metabox() {

		add_meta_box(
			'charitypress-logs',
			_x( 'Log Content', 'Log', 'charitypress' ),
			array( $this, 'metabox_callback' ),
			self::POST_TYPE,
			'normal',
			'core'
		);

	}

	public function metabox_callback( $post ) {

		$log = get_post_meta( $post->ID, self::META_KEY, true );
		echo wp_kses_post( $log );

	}

	public function register_post_type() {
		$labels = array(
			'name'               => _x( 'Logs', 'Post Type General Name', 'charitypress' ),
			'singular_name'      => _x( 'Log', 'Post Type Singular Name', 'charitypress' ),
			'menu_name'          => __( 'Log', 'charitypress' ),
			'parent_item_colon'  => __( 'Parent Log:', 'charitypress' ),
			'all_items'          => __( 'All Logs', 'charitypress' ),
			'view_item'          => __( 'View Log', 'charitypress' ),
			'add_new_item'       => __( 'Add New Log', 'charitypress' ),
			'add_new'            => __( 'Add New', 'charitypress' ),
			'edit_item'          => __( 'Edit Log', 'charitypress' ),
			'update_item'        => __( 'Update Log', 'charitypress' ),
			'search_items'       => __( 'Search Log', 'charitypress' ),
			'not_found'          => __( 'Not found', 'charitypress' ),
			'not_found_in_trash' => __( 'Not found in Trash', 'charitypress' ),
		);

		$args = array(
			'label'               => __( 'charitypress_log', 'charitypress' ),
			'description'         => __( 'Logger for CharityPress', 'charitypress' ),
			'labels'              => $labels,
			'supports'            => array( 'title', ),
			'hierarchical'        => false,
			'public'              => false,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_nav_menus'   => true,
			'show_in_admin_bar'   => true,
			'menu_position'       => 5,
			'can_export'          => false,
			'has_archive'         => false,
			'exclude_from_search' => false,
			'publicly_queryable'  => false,
			'capability_type'     => 'post',
		);
		register_post_type( self::POST_TYPE, $args );


	}

	/**
	 * Register the team taxonomy
	 */
	public function register_taxonomy() {

		register_taxonomy(
			self::TAXONOMY_NAME,
			self::POST_TYPE,
			array(
				'public'       => true,
				'hierarchical' => true,
				'labels'       => array(
					'name'                       => __( 'Log Types' ),
					'singular_name'              => __( 'Log Type' ),
					'menu_name'                  => __( 'Log Types' ),
					'search_items'               => __( 'Search Log Types' ),
					'popular_items'              => __( 'Popular Log Types' ),
					'all_items'                  => __( 'All Log Types' ),
					'edit_item'                  => __( 'Edit Log Type' ),
					'update_item'                => __( 'Update Log Type' ),
					'add_new_item'               => __( 'Add New Log Type' ),
					'new_item_name'              => __( 'New Log Type' ),
					'separate_items_with_commas' => __( 'Separate log types with commas' ),
					'add_or_remove_items'        => __( 'Add or remove log types' ),
					'choose_from_most_used'      => __( 'Choose from the most popular log types' ),
				),
				'capabilities' => array(
					'manage_terms' => false,
					'edit_terms'   => false,
					'delete_terms' => false,
					'assign_terms' => false,

				)
			)
		);
	}

	function filter_by_type() {

		// only display these taxonomy filters on desired custom post_type listings
		global $typenow;
		if ( $typenow == self::POST_TYPE ) {

			// create an array of taxonomy slugs you want to filter by - if you want to retrieve all taxonomies, could use get_taxonomies() to build the list
			$filters = array( self::TAXONOMY_NAME );

			foreach ( $filters as $tax_slug ) {
				// retrieve the taxonomy object
				$tax_obj  = get_taxonomy( $tax_slug );
				$tax_name = $tax_obj->labels->name;
				// retrieve array of term objects per taxonomy
				$terms = get_terms( $tax_slug );

				// output html for taxonomy dropdown filter
				echo "<select name='$tax_slug' id='$tax_slug' class='postform'>";
				echo "<option value=''>Show All $tax_name</option>";
				foreach ( $terms as $term ) {
					// output each select option line, check against the last $_GET to show the current option selected
					echo '<option value=' . $term->slug, $_GET[ $tax_slug ] == $term->slug ? ' selected="selected"' : '', '>' . $term->name . ' (' . $term->count . ')</option>';
				}
				echo "</select>";
			}
		}
	}

	public function taxonomy_column( $columns ) {
		if ( ! is_admin() ) {
			return;
		}

		unset ( $columns['date'] );
		$columns['log_taxonomy'] = __( 'Log Type', 'charitypress' );
		$columns['date']                  = 'Date';

		return $columns;
	}

	public function taxonomy_column_content( $column_name, $post_id ) {
		if ( ! is_admin() ) {
			return;
		}
		if ( 'log_taxonomy' === $column_name ) {
			if ( is_array( $terms = wp_get_object_terms($post_id, self::TAXONOMY_NAME )) ) {
				foreach($terms as $index => $term){
					echo ucwords( esc_html( $term->name ) );
					if ( $index > 0 ){
						echo '<br>';
					}

				}
			}
		}
	}


}

$cpl = new Charity_Post_Logger_Post_Type;
$cpl->init();


