<?php

class Charity_User_Profile_Fields {

	/**
	 * Set up class
	 */
	public function init() {

		$this->add_field_hooks();

	}

	/**
	 * Attach hooks
	 */
	public function add_field_hooks() {

		add_action( 'show_user_profile', array( $this, 'add_fields' ) );
		add_action( 'edit_user_profile', array( $this, 'add_fields' ) );
		add_action( 'personal_options_update', array( $this, 'save_fields' ) );
		add_action( 'edit_user_profile_update', array( $this, 'save_fields' ) );

	}

	/**
	 * Add fields for user types
	 *
	 * @param $user
	 */
	public function add_fields( $user ) {
		$proceed = $this->correct_user_type( $user );
		if ( false === $proceed ) {
			return;
		}


		?><h3><?php _e( 'CharityPress Profile Information', 'charitypress' ); ?></h3>

		<table class="form-table">

			<?php
			$this->output_fields_for_everyone( $user );
			if ( current_user_can( 'edit_users' ) ) {
				$this->output_fields_for_admins( $user );
			}
			?>

		</table>
	<?php
	}

	/**
	 * Check if the user is one of the charity users
	 *
	 * @param $user
	 *
	 * @return bool
	 */
	public function correct_user_type( $user ) {

		$crm     = new Charity_Role_Manager;
		$roles   = array_keys( $crm->get_roles() );
		$proceed = false;
		foreach ( $roles as $role ) {
			if ( in_array( $role, $user->roles ) ) {
				$proceed = true;
			}
		}

		return $proceed;
	}

	/**
	 * Output fields
	 *
	 * @param $user
	 */
	public function output_fields_for_everyone( $user ) {
		if ( ! $this->correct_user_type( $user ) ) {
			return;
		}
		if ( current_user_can( 'is_pledger' ) || current_user_can( 'is_donor' ) || current_user_can( 'manage_options' ) ) {
			?>
			<tr>
				<th>
					<label for="pledge-per-noun"><?php _e( 'Amount pledged per ' . Charity_Settings::get( 'singular_noun', '{{NOUN}}' ), 'charitypress' ); ?></label>
				</th>

				<td>
					<input type="text" name="pledge-per-noun" id="pledge-per-noun" value="<?php echo number_format( (float) get_user_meta( $user->ID, 'pledge-per-noun', true ), 2 ); ?>" class="regular-text" /><br />
					<span class="description"><?php _e( 'In Dollars', 'charitypress' ); ?></span>
				</td>
			</tr>
		<?php } ?>
		<tr>
			<th>
				<label for="team"><?php _e( 'Team', 'charitypress' ); ?></label>

			</th>

			<td>
				<?php
				$terms = get_terms( Charity_Team_User_Taxonomy::TAXONOMY_NAME, array( 'hide_empty' => false ) );
				if ( count( $terms ) ) {
					$team_on_slug        = null;
					$team_linked_objects = wp_get_object_terms( $user->ID, Charity_Team_User_Taxonomy::TAXONOMY_NAME );
					if ( is_array( $team_linked_objects ) && count( $team_linked_objects ) ) {
						$team_on      = $team_linked_objects[0];
						$team_on_slug = $team_on->slug;
					}
					?>
					<select name="team">
					<option value="">-- No Team --</option><?php
					foreach ( $terms as $term ) {
						?>
						<option id="team-<?php echo esc_attr( $term->slug ); ?>" value="<?php echo esc_attr( $term->slug ); ?>" <?php selected( true, $term->slug === $team_on_slug ); ?>><?php echo esc_html( $term->name ); ?></option><?php
					}
					?></select><?php
				} else {
					_e( 'No teams found', 'charity' );
				}

				add_thickbox();
				if ( current_user_can( 'is_pledger' ) || current_user_can( 'is_donor' ) ) {

					$cstn = new Charity_Suggest_Team_Name;
					$cstn->output_suggest();


				} ?>
			</td>
		</tr>

		<tr>
			<th>
				<label for="anonymous"><?php _e( 'Anonymous', 'charitypress' ); ?></label>
			</th>

			<td>
				<input type="checkbox" name="anonymous" id="anonymous" <?php checked( 'on', get_user_meta( $user->ID, 'anonymous', true ) ); ?> />

			</td>
		</tr>
		<?php if ( ! current_user_can( 'edit_users' ) ) { ?>
			<tr>
				<th>
					<label for="total-donated"><?php _e( 'Total Donated', 'charitypress' ); ?></label>
				</th>

				<td>
					<input type="text" readonly name="total-donated" id="total-donated" value="<?php echo number_format( (float) esc_attr( get_user_meta( $user->ID, 'total-donated', true ) ), 2 ); ?>" class="regular-text" /><br />
					<span class="description"><?php _e( 'In Dollars', 'charitypress' ); ?></span>
				</td>
			</tr>

		<?php } ?>
	<?php
	}

	/**
	 * Output admin fields
	 *
	 * @param $user
	 */
	public function output_fields_for_admins( $user ) {
		if ( ! current_user_can( 'edit_user', $user->ID ) ) {
			return;
		}
		?>
		<tr>
			<th>
				<label for="charitypress-active"><?php _e( 'User Active', 'charitypress' ); ?></label>
			</th>

			<td>
				<input type="checkbox" name="charitypress-active" id="charitypress-active" <?php checked( true, get_user_meta( $user->ID, 'charitypress-active', true ) ); ?> />

			</td>
		</tr>
		<tr>
			<th>
				<label for="is-captain"><?php _e( 'Captain?', 'charitypress' ); ?></label>
			</th>

			<td>
				<input type="checkbox" name="is-captain" id="is-captain" <?php checked( 'on', get_user_meta( $user->ID, 'is-captain', true ) ); ?> />

			</td>
		</tr>
		<tr>
			<th>
				<label for="total-donated"><?php _e( 'Total Donated', 'charitypress' ); ?></label>
			</th>

			<td>
				<input type="text" name="total-donated" id="total-donated" value="<?php echo number_format( (float) get_user_meta( $user->ID, 'total-donated', true ), 2 ); ?>" class="regular-text" /><br />
				<span class="description"><?php _e( 'In Dollars', 'charitypress' ); ?></span>
				<span><?php echo wp_kses_post( __( 'By editing this you will permanently change the amount donated by this user and your payment processor (e.g. Stripe) may not reconcile as expected.', 'charitypress' ) ); ?></span>
			</td>
		</tr>
		<tr>
			<th>
				<label for="customer-token"><?php _e( 'Payment Token', 'charitypress' ); ?></label>
			</th>

			<td>
				<input type="text" name="customer-token" id="customer-token" value="<?php echo esc_attr( get_user_meta( $user->ID, 'customer-token', true ) ); ?>" class="regular-text" /><br />
				<span class="description"><strong><?php _e( 'Only change if you know what you are doing.', 'charitypress' ); ?></strong></span>
			</td>
		</tr>
		<tr>
			<th>
				<label for="charge-log"><?php _e( 'Charge Log', 'charitypress' ); ?></label>
			</th>

			<td>
				<?php

				$user_log = get_user_meta( $user->ID, Charity_User_Logger::USER_META_KEY, true );
				if ( ! $user_log ) {
					$user_log = __( '(No charges yet)', 'charitypress' );
				}
				echo $user_log;

				?>
			</td>
		</tr>
	<?php
	}

	/**
	 * Update settings for user
	 *
	 * @param $user_id
	 */
	public function save_fields( $user_id ) {
		if ( ! $this->correct_user_type( get_user_by( 'id', $user_id ) ) ) {
			return;
		}

		update_user_meta( $user_id, 'pledge-per-noun', (float) $_POST['pledge-per-noun'] );
		update_user_meta( $user_id, 'anonymous', ( isset( $_POST['anonymous'] ) && 'on' === $_POST['anonymous'] ) ? 'on' : null );
		wp_set_object_terms( $user_id, array( esc_attr( $_POST['team'] ) ), Charity_Team_User_Taxonomy::TAXONOMY_NAME, false );

		clean_object_term_cache( $user_id, Charity_Team_User_Taxonomy::TAXONOMY_NAME );

		if ( current_user_can( 'edit_users' ) ) {
			update_user_meta( $user_id, 'total-donated', (float) $_POST['total-donated'] );
			update_user_meta( $user_id, 'is-captain', ( isset( $_POST['is-captain'] ) && 'on' === $_POST['is-captain'] ) ? 'on' : null );
			update_user_meta( $user_id, 'customer-token', (string) $_POST['customer-token'] );
			update_user_meta( $user_id, 'charitypress-active', ( isset( $_POST['charitypress-active'] ) && 'on' === $_POST['charitypress-active'] ) ? true : false );
		}

	}


}

$cwpf = new Charity_User_Profile_Fields();
$cwpf->init();