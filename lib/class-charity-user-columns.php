<?php

class Charity_User_Header {

	/**
	 * Set up class
	 */
	public function init() {

		$this->attach_hooks();

	}

	/**
	 * attach hooks
	 */
	public function attach_hooks() {

		add_filter( 'manage_users_columns', array( $this, 'manage_columns' ) );
		add_filter( 'manage_users_custom_column', array( $this, 'add_column_data' ), 10, 3 );
		add_filter( 'manage_users_sortable_columns', array( $this, 'manage_columns_sortable' ) );


	}

	/**
	 * Add column for user screen's table
	 *
	 * @param $column
	 *
	 * @return mixed
	 */
	public function manage_columns( $column ) {
		if ( ! apply_filters( 'charitypress_show_posts_count', false ) ) {
			unset( $column['posts'] );
		} //lets get rid of `posts` column for users

		$column['team']            = __( 'Team', 'charitypress' );
		$column['total-donated']   = __( 'Total Donated', 'charitypress' );
		$column['pledge-per-noun'] = __( 'Pledged per ' . Charity_Settings::get( 'singular_noun', '{{NOUN}}' ), 'charitypress' );
		$column['status']          = __( 'Status', 'charitypress' );

		return $column;
	}

	/**
	 * Add column for user screen's table
	 *
	 * @param $column
	 *
	 * @return mixed
	 */
	public function manage_columns_sortable( $column ) {
		if ( ! apply_filters( 'charitypress_show_posts_count', false ) ) {
			unset( $column['posts'] );
		} //lets get rid of `posts` column for users

		$column['team']            = 'team';
		$column['total-donated']   = 'total-donated';
		$column['pledge-per-noun'] = 'pledge-per-noun';
		$column['active']          = 'status';

		return $column;
	}

	/**
	 * Add the data for the newly created column
	 *
	 * @param $val
	 * @param $column_name
	 * @param $user_id
	 *
	 * @return null|string|void
	 */
	function add_column_data( $val, $column_name, $user_id ) {

		switch ( $column_name ) {
			case 'team' :
				$team_linked_objects = wp_get_object_terms( $user_id, Charity_Team_User_Taxonomy::TAXONOMY_NAME );
				if ( is_array( $team_linked_objects ) && count( $team_linked_objects ) ) {
					$team_on = $team_linked_objects[0];

					return $team_on->name;
				}

				return __( '(no team)' );

				break;
			case 'total-donated' :
				$total_donated = number_format( (float) get_user_meta( $user_id, 'total-donated', true ), 2 );

				return $total_donated;

				break;

			case 'pledge-per-noun' :
				if ( user_can( $user_id, 'edit_user' ) ) {
					return 'Admin';
				}
				if ( user_can( $user_id, 'is_donor' ) ) {
					return 'N/A';
				}
				$total_pledged = number_format( (float) get_user_meta( $user_id, 'pledge-per-noun', true ), 2 );

				return $total_pledged;

				break;
			case 'status' :

				if ( user_can( $user_id, 'edit_user' ) ) {
					return 'Admin';
				}

				return get_user_meta( $user_id, 'charitypress-active', true ) ? 'Active' : 'Inactive';


				break;
			default:
		}

		return null;
	}

}

$cuh = new Charity_User_Header;
$cuh->init();

add_action( 'pre_get_users', function ( $query ) {

	if ( ! is_admin() ) {
		return;
	}

	global $pagenow;

	if ( 'users.php' !== $pagenow ) {
		return;
	}

	$orderby     = $query->query_vars['orderby'];
	$order_types = array(
		'total-donated'   => 'meta_value',
		'pledge-per-noun' => 'meta_value',
		'status'          => 'meta_value'
	);

	if ( isset( $query->query_vars['orderby'] ) && in_array( $orderby, array_keys( $order_types ) ) ):

		$query->query_vars['orderby']  = $order_types[ $orderby ];
		$query->query_vars['meta_key'] = $orderby;

	endif;

} );
