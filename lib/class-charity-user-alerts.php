<?php

class Charity_User_Alerts {


	private $text_domain = 'charitypress';
	const OPTION_NAME = 'charityalerts';

	/**
	 * Private constructor for singleton
	 */
	protected function __construct() {

	}

	/**
	 * Get instance of singleton
	 * @return static
	 */
	public static function getInstance() {
		static $instance = null;
		if ( null === $instance ) {
			$instance = new static();
		}

		return $instance;
	}

	/**
	 * Initialize the class
	 */
	public function init() {

		$this->attach_hooks();

	}

	/**
	 * Attach hooks
	 */
	public function attach_hooks() {

		add_action( 'admin_notices', array( $this, 'alerts' ), PHP_INT_MAX );

	}

	/**
	 * Build update and error notices
	 */
	public function alerts() {

		if ( ! is_admin() ) {
			return;
		}
		$alerts = $this->get_alerts();
		if ( ! count( $alerts ) ) {
			return;
		}

		foreach ( $alerts as $index => $alert ) {

			$alert_text = $alert['alert_text'];
			$type       = $alert['type'];
			$template   = '<div class="%s"><p>' . __( '%s', $this->text_domain ) . '</p></div>';
			if ( isset( $alert['user_id'] ) && $alert['user_id'] !== get_current_user_id() ) {
				continue;
			}
			printf( $template, $type, $alert_text );
			unset( $alerts[ $index ] );

		}
		if ( ! $alerts ) {
			$alerts = array();
		}
		$this->set_alerts( $alerts );

	}

	public function get_alerts() {

		return get_option( self::OPTION_NAME, array() );

	}

	/**
	 * Add update (or generic, based on $type) notice
	 *
	 * @param        $alert
	 * @param        $user_id
	 */
	public function add_notice( $alert, $user_id = null ) {

		$this->add_message( $alert, 'updated', $user_id );

	}

	/**
	 * Add error notice
	 *
	 * @param $alert
	 * @param $user_id
	 */
	public function add_error( $alert, $user_id = null ) {

		$this->add_message( $alert, 'error', $user_id );

	}

	private function add_message( $message, $type, $user_id = null ) {

		$alerts   = $this->get_alerts();
		$alerts[] = array(
			'type'       => $type,
			'alert_text' => $message,
			'user_id'    => $user_id
		);
		$this->set_alerts( $alerts );

	}

	private function set_alerts( $alerts ) {
		update_option( self::OPTION_NAME, $alerts );
	}

}

Charity_User_Alerts::getInstance()->init();
