<?php

class Charity_Email {


	private $success = false;
	private $status = array();

	/**
	 * @param        $subject
	 * @param        $message
	 * @param string $headers
	 * @param array  $attachments
	 *
	 * @return bool
	 */
	public function email_admins( $subject, $message, $headers = '', $attachments = array() ) {

		return wp_mail( $this->get_admin_email_addresses(), $subject, $message, $headers, $attachments );

	}

	public function send_new_user_email( $data ) {
		$user               = get_user_by( 'id', $data['user_id'] );
		$user_email_address = $data['email'];
		$password           = $data['password'];
		$name               = $data['first_name'] . ' ' . $data['last_name'];
		if ( Charity_Role_Manager::RECURRING_ROLE === $data['type'] ) {
			$subject             = Charity_Settings::get( 'email_subject_after_initial_pledge' );
			$amount_template_tag = '{amount_pledged}';
		} elseif ( Charity_Role_Manager::ONE_TIME_ROLE === $data['type'] ) {
			$subject             = Charity_Settings::get( 'email_subject_after_direct_donation' );
			$amount_template_tag = '{amount_donated}';
		} else {
			//@todo log error
			return;
		}

		if ( Charity_Role_Manager::RECURRING_ROLE === $data['type'] ) {
			$body = Charity_Settings::get( 'email_body_after_initial_pledge' );
		} elseif ( Charity_Role_Manager::ONE_TIME_ROLE === $data['type'] ) {
			$body = Charity_Settings::get( 'email_body_after_direct_donation' );
		}
		$admin_subject         = ' (ADMIN COPY) ' . $subject;
		$replace               = array(
			'{login}',
			'{password}',
			'{name}',
			'{login_url}',
			'{site_name}',
			$amount_template_tag,
			'{singular_noun}',
			'{plural_noun}',
			'{present_tense_verb}',
			'{past_tense_verb}'
		);
		$replace_with_user     = array(
			$user_email_address,
			$password,
			$name,
			wp_login_url(),
			get_bloginfo( 'name' ),
			number_format( (float) $data['amount'], 2 ),
			Charity_Settings::get( 'singular_noun' ),
			Charity_Settings::get( 'plural_noun' ),
			Charity_Settings::get( 'present_tense_verb' ),
			Charity_Settings::get( 'past_tense_verb' )
		);
		$replace_with_admin    = $replace_with_user;
		$replace_with_admin[1] = '{redacted}';
		$user_body             = str_replace( $replace, $replace_with_user, $body );
		$admin_body            = str_replace( $replace, $replace_with_admin, $body );
		add_filter( 'wp_mail_content_type', array( $this, 'set_html_content_type' ) );
		$this->success = wp_mail( $user_email_address, $subject, $user_body );

		if ( true === Charity_Settings::get( 'email_admins_user_emails' ) ) {
			$this->success = $this->email_admins( $admin_subject, $admin_body );
			$notice = sprintf( '%s - sent email for user creation for donor %s (%s)',date( 'm/d/Y  H:i:s', time() + get_option( 'gmt_offset' ) * 3600 ), $user->data->display_name, $user->data->user_email );
			$data['post_logger']->log_notice( $notice, $notice );
			$data['user_logger']->log( $notice );
		}


		remove_filter( 'wp_mail_content_type', array( $this, 'set_html_content_type' ) );


	}

	public static function set_html_content_type() {
		return 'text/html';
	}

	/**
	 * Filter:
	 * charitypress_admin_emails - allows you to filter out the admin email addresses, by email address
	 * charitpyress_admin_role - allows you to specify the admin role.
	 * @return array
	 */
	public function get_admin_email_addresses() {

		$emails = array();
		$users  = get_users( array( 'role' => apply_filters( 'charitpyress_admin_role', 'administrator' ) ) );
		foreach ( $users as $user ) {
			$emails[] = $user->user_email;
		}
		$emails = apply_filters( 'charitypress_admin_emails', $emails );

		return $emails;

	}

	public function status() {

		$result          = new stdClass();
		$result->success = $this->success;
		$result->status  = $this->status;
		if ( ! $result->success && count( $result->status ) ) {
			$result->status = array( _x( 'Generic Error.', 'email error', 'charitypress' ) );
		}

		return $result;

	}

	public function queue_periodic_charge_email( WP_User $user, $data ) {

		$name            = $user->data->display_name;
		$login_url       = wp_login_url();
		$site_name       = get_bloginfo( 'name' );
		$amount_charged  = $data['amount_charged'];
		$nouns_in_period = $data['nouns_in_period'];
		$site_url        = get_home_url();

		$replace      = array(
			'{name}',
			'{login_url}',
			'{site_name} ',
			'{amount_charged} ',
			'{nouns_in_period}',
			'{site_url}',
			'{team_info}'
		);
		$replace_with = array(
			$name,
			$login_url,
			$site_name,
			$amount_charged,
			$nouns_in_period,
			$site_url,
			$this->build_team_info( $user )
		);
		$subject      = Charity_Settings::get( 'email_subject_after_periodic_pledge' );
		$body         = Charity_Settings::get( 'email_body_after_periodic_pledge' );
		$body         = str_replace( $replace, $replace_with, $body );

		Charity_Email_Queue::add( $user->data->user_email, $subject, $body, $user );


	}

	public function build_team_info( WP_User $user ) {
		$team_name           = null;
		$team_linked_objects = wp_get_object_terms( $user->ID, Charity_Team_User_Taxonomy::TAXONOMY_NAME );
		$team_on             = null;
		$count_on_team       = 0;
		$team_slug           = null;
		if ( is_array( $team_linked_objects ) && count( $team_linked_objects ) ) {
			$team_on        = $team_linked_objects[0];
			$team_name      = $team_on->name;
			$team_slug      = $team_on->slug;
			$term           = get_term_by( 'slug', $team_on->slug, Charity_Team_User_Taxonomy::TAXONOMY_NAME );
			$term_id        = absint( $term->term_id );
			$object_in_term = get_objects_in_term( $term_id, Charity_Team_User_Taxonomy::TAXONOMY_NAME );
			$count_on_team  = count( $object_in_term );

		}
		$team_info_template = Charity_Settings::get( 'team_info' );
		if ( ! $team_info_template || ! $team_on ) {
			return null;
		}

		$replace      = array(
			'{team_name}',
			'{team_amount}',
			'{team_number_of_members}'
		);
		$replace_with = array(
			$team_name,
			sprintf( '{team_amount slug="%s"}', $team_slug ),
			$count_on_team,


		);

		return str_replace( $replace, $replace_with, $team_info_template );

	}

	public static function wp_mail( $user_id, $to, $subject, $message, $headers = '', $attachments = array() ) {
		$user_type   = user_can( $user_id, 'is_donor' ) ? Charity_Role_Manager::ONE_TIME_ROLE : Charity_Role_Manager::RECURRING_ROLE;
		$user        = get_user_by( 'id', $user_id );
		$user_logger = new Charity_User_Logger( $user );
		$post_logger = new Charity_Post_Logger;
		$message     = self::add_totals( $message, $user_id );
		$message     = stripslashes( $message );
		$subject     = stripslashes( $subject );
		add_filter( 'wp_mail_content_type', array( __CLASS__, 'set_html_content_type' ) );
		$email        = wp_mail( $to, $subject, $message, $headers = '', $attachments = array() );
		$email_admins = Charity_Settings::get( 'email_admins_user_emails' );
		if ( $email_admins ) {
			$admin_email_addresses = array();
			$users                 = get_users( array( 'role' => apply_filters( 'charitpyress_admin_role', 'administrator' ) ) );
			foreach ( $users as $user ) {
				$admin_email_addresses[] = $user->user_email;
			}
			$admin_email_addresses = apply_filters( 'charitypress_admin_emails', $admin_email_addresses );
			wp_mail( $admin_email_addresses, '(ADMIN COPY) ' . $subject, $message, $headers = '', $attachments = array() );
		}
		remove_filter( 'wp_mail_content_type', array( __CLASS__, 'set_html_content_type' ) );
		$response  = new stdClass();
		$user_type = user_can( $user_id, 'is_donor' ) ? Charity_Role_Manager::ONE_TIME_ROLE : Charity_Role_Manager::RECURRING_ROLE;
		if ( $email ) {
			$response->statusMessage = date( 'm/d/Y  H:i:s', time() + get_option( 'gmt_offset' ) * 3600 ) . ' - Successfully emailed ' . $to . ' (' . $user_type . ')' . sprintf( ' <a href="%s">[Edit User]</a>', get_edit_user_link( $user_id ) );
			$user_logger->log( $response->statusMessage );
			$post_logger->log_notice( 'Email:<br>' . $message, date( 'm/d/Y  H:i:s', time() + get_option( 'gmt_offset' ) * 3600 ) . '  (' . $user_type . ') ' . $to . ' email sent successfully' );
		} else {
			$response->statusMessage = date( 'm/d/Y  H:i:s', time() + get_option( 'gmt_offset' ) * 3600 ) . ' - Failure emailing ' . $to . ' (' . $user_type . ')' . sprintf( ' <a href="%s">[Edit User]</a>', get_edit_user_link( $user_id ) );
			$user_logger->log( $response->statusMessage );
			$post_logger->log_notice( 'Email:<br>' . $message, date( 'm/d/Y  H:i:s', time() + get_option( 'gmt_offset' ) * 3600 ) . '  (' . $user_type . ') ' . $to . ' email failed' );
		}

		return $response;

	}

	/**
	 * Populate totals for team, user and campaign
	 *
	 * @param $message
	 * @param $user_id
	 *
	 * @return string
	 */
	public static function add_totals( $message, $user_id ) {

		$totals         = Charity_Totals::get_totals();
		$campaign_total = $totals['total'];

		$message = str_replace( '{campaign_total}', $campaign_total, $message );
		$message = str_replace( '{total_pledged}', $totals['users'][ $user_id ], $message );

		$team_total = 0;
		$pattern    = '/(\\{)(team_amount)( )(slug)(=)(.)(")((?:[a-z][a-z]+))(.)(")(\\})/is';
		if ( preg_match_all( $pattern, $message, $matches ) ) {
			$slug = $matches[8][0];
			if ( array_key_exists( $slug, $totals['teams'] ) ) {
				$team_total = (float) $totals['teams'][ $slug ];
			}

		}


		$message = preg_replace( $pattern, $team_total, $message );

		return $message;

	}


}