<?php


class Charity_Team_User_Taxonomy {

	const TAXONOMY_NAME = 'team';

	/**
	 * Set up class
	 */
	public function init() {

		$this->attach_hooks();

	}

	/**
	 * Attach hooks for the class
	 */
	public function attach_hooks() {

		add_action( 'init', array( $this, 'register_taxonomy' ), 0 );
		add_action( 'admin_menu', array( $this, 'admin_page' ) );
		add_filter( 'parent_file', array( $this, 'fix_tax_page' ) );
		add_action( 'deleted_user', array( $this, 'update_counts' ) ); //fix the counts on deletion, registration and update of users
		add_action( 'user_register', array( $this, 'update_counts' ) ); //fix the counts on deletion, registration and update of users
		add_action( 'profile_update', array( $this, 'update_counts' ) ); //fix the counts on deletion, registration and update of users

	}

	/**
	 * Register the team taxonomy
	 */
	public function register_taxonomy() {

		register_taxonomy(
			self::TAXONOMY_NAME,
			'user',
			array(
				'public'                => true,
				'labels'                => array(
					'name'                       => __( 'Teams' ),
					'singular_name'              => __( 'Team' ),
					'menu_name'                  => __( 'Teams' ),
					'search_items'               => __( 'Search Teams' ),
					'popular_items'              => __( 'Popular Teams' ),
					'all_items'                  => __( 'All Teams' ),
					'edit_item'                  => __( 'Edit Team' ),
					'update_item'                => __( 'Update Team' ),
					'add_new_item'               => __( 'Add New Team' ),
					'new_item_name'              => __( 'New Team' ),
					'separate_items_with_commas' => __( 'Separate teams with commas' ),
					'add_or_remove_items'        => __( 'Add or remove teams' ),
					'choose_from_most_used'      => __( 'Choose from the most popular teams' ),
				),
				'rewrite'               => array(
					'with_front' => true,
					'slug'       => 'team'
				),
				'capabilities'          => array(
					'manage_terms' => 'edit_users',
					'edit_terms'   => 'edit_users',
					'delete_terms' => 'edit_users',
					'assign_terms' => 'read',
				),
				'update_count_callback' => array( $this, 'update_count' )
			)
		);
	}

	/**
	 * Update user count for taxonomy
	 *
	 * @param $terms
	 * @param $taxonomy
	 */
	public function update_count( $terms, $taxonomy ) {
		global $wpdb;

		foreach ( (array) $terms as $term ) {

			$count = $wpdb->get_var( $wpdb->prepare( "SELECT COUNT(*) FROM $wpdb->term_relationships WHERE term_taxonomy_id = %d", $term ) );

			do_action( 'edit_term_taxonomy', $term, $taxonomy );
			$wpdb->update( $wpdb->term_taxonomy, compact( 'count' ), array( 'term_taxonomy_id' => $term ) );
			do_action( 'edited_term_taxonomy', $term, $taxonomy );
		}

	}

	public function update_counts() {
		$this->clean_counts();
		global $wpdb;
		$term_taxonomy_ids = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM $wpdb->term_taxonomy WHERE taxonomy = %s", self::TAXONOMY_NAME ) );
		foreach ( $term_taxonomy_ids as $term_taxonomy_id ) {
			$term  = $term_taxonomy_id->term_taxonomy_id;
			$count = $wpdb->get_var( $wpdb->prepare( "SELECT COUNT(*) FROM $wpdb->term_relationships WHERE term_taxonomy_id = %d", $term ) );
			do_action( 'edit_term_taxonomy', $term, self::TAXONOMY_NAME );
			$wpdb->update( $wpdb->term_taxonomy, compact( 'count' ), array( 'term_taxonomy_id' => $term ) );
			do_action( 'edited_term_taxonomy', $term, self::TAXONOMY_NAME );
		}
		$this->clean_counts();

	}

	public function clean_counts() {
		global $wpdb;
		$terms = $wpdb->get_results( $wpdb->prepare( "select * from $wpdb->term_relationships left join $wpdb->term_taxonomy on $wpdb->term_relationships.term_taxonomy_id = $wpdb->term_taxonomy.term_taxonomy_id where taxonomy=%s", self::TAXONOMY_NAME ) );
		foreach ( $terms as $term ) {
			$object_id = $term->object_id;
			if ( ! get_user_by( 'id', $object_id ) ) {
				$wpdb->delete( $wpdb->term_relationships, array( 'object_id' => $term->object_id ) );
			}

		}

	}

	/**
	 * Add management page for taxonomy to user menu
	 */
	public function admin_page() {
		$tax = get_taxonomy( self::TAXONOMY_NAME );

		add_users_page(
			esc_attr( $tax->labels->menu_name ),
			esc_attr( $tax->labels->menu_name ),
			$tax->cap->manage_terms,
			'edit-tags.php?taxonomy=' . $tax->name
		);
	}

	/**
	 * Correct where user menu shows up
	 *
	 * @param string $parent_file
	 *
	 * @return string
	 */
	function fix_tax_page( $parent_file = '' ) {
		global $pagenow;

		if ( ! empty( $_GET['taxonomy'] ) && $_GET['taxonomy'] == self::TAXONOMY_NAME && $pagenow == 'edit-tags.php' ) {
			$parent_file = 'users.php';
		}

		return $parent_file;
	}


}

$ctut = new Charity_Team_User_Taxonomy();
$ctut->init();

