<?php

class Charity_Post_Logger {


	private function log( $message, $subject, $term ) {

		$this->create_term( $term );
		if ( $subject ) {
			$title = $subject;
		} else {
			$title = current_time( 'mysql' );
		}

		$post_id = wp_insert_post( array(
			'post_title'  => strip_tags( $title ),
			'post_type'   => Charity_Post_Logger_Post_Type::POST_TYPE,
			'post_status' => 'publish'
		) );
		wp_set_object_terms( $post_id, $term, Charity_Post_Logger_Post_Type::TAXONOMY_NAME );
		update_post_meta( $post_id, Charity_Post_Logger_Post_Type::META_KEY, $message );
	}

	public function log_error( $message, $subject = null ) {

		$this->log( $message, $subject, 'error' );

	}

	public function log_notice( $message, $subject = null ) {

		$this->log( $message, $subject, 'notice' );


	}

	public function create_term( $term ) {
		if ( ! term_exists( $term, Charity_Post_Logger_Post_Type::TAXONOMY_NAME ) ) {
			wp_create_term( $term, Charity_Post_Logger_Post_Type::TAXONOMY_NAME );
		}
	}


}

