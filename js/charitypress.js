/*global window,document,jQuery,stripejs,Stripe */

jQuery(document).ready(function ($) {
	'use strict';

	var validateEmail = function (email) {
		var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(email);
	};

	$('form.charitypress-form').validationEngine();

	var process = function ($form, formId) {
		Stripe.card.createToken($form, function (status, response) {

			var $form = $('.charitypress-form'),
				$message = $form.find('.message');

			$form.css('opacity', '1');
			if (response.error) {
				$message.show().text(response.error.message);
				$form.find('button[type="submit"]').remoteAttr('disabled');
			} else {
				var token = response.id;
				$form.find('button[type="submit"]').hide();
				$form.append($('<input type="hidden" name="stripeToken" />').val(token));
				$form.submit();


			}

		});


	};


	$(document).on('submit', 'form.charitypress-form', function (e) {
		e.preventDefault();
		var $form = $(this),
			formId = $form.attr('id'),
			$message = $form.find('.message'),
			$token = $form.find('input[name="stripeToken"]'),
			token = $token.val(),
			$this = $(this);
		$form.css('opacity', '0.5');
		$message.html('');
		$message.hide();

		if ($token.length === 0) {
			$form.find('button[type="submit"]').prop('disabled', true);

			process($form, formId);
		} else if ($token.length > 0) {
			var $firstName = $form.find('.first-name'),
				firstName = $firstName.val(),
				$lastName = $form.find('.last-name'),
				lastName = $lastName.val(),
				$type = $form.find('.type'),
				type = $type.val(),
				$amount = $form.find('.amount'),
				amount = $amount.val(),
				$anonymous = $form.find('.anonymous'),
				anonymous = $anonymous.is(':checked'),
				$email = $form.find('.email'),
				email = $email.val(),
				$team = $form.find('.team'),
				team = $team.val(),
				$nonce = $form.find('.nonce'),
				nonce = $nonce.val(),
				ajaxurl = charitypress.ajaxurl,
				data = {
					action   : 'charitypress_initialize_user',
					firstName: firstName,
					lastName : lastName,
					email    : email,
					amount   : amount,
					token    : token,
					team     : team,
					anonymous: anonymous,
					nonce    : nonce,
					type     : type
				};

			$.post(ajaxurl, data, function (response) {
				var result = 'Failure';
				if (response.success) {
					result = 'Success';
				}
				$message.show();
				var statii = '';
				for (var i = 0; i < response.status.length; i++) {
					statii += '<p>' + response.status[i] + '</p>';
				}

				$form.css('opacity', '1');
				if (!response.success) {
					$form.find('button[type="submit"]').show();
					$message.html(result + statii);
					$form.find('[name="stripeToken"]').remove();
					$form.find('button[type="submit"]').prop('disabled', false);
				} else {
					window.location.replace(response.url);
				}
			});


		}
	});

	$('#charitypress-subscribe-button').on('click', function (e) {
		var $input = $('#charitypress-subscribe-email-address'),
			valid = validateEmail($input.val()),
			email = $input.val(),
			$parent = $(this).closest('.charitypress-subscribe-form'),
			$message = $parent.find('.message'),
			$nonce = $parent.find('.nonce'),
			ajaxurl = charitypress.ajaxurl,
			nonce = $nonce.val();
		e.preventDefault();
		$input.removeClass('invalid');
		$message.html('');
		$message.removeClass('error');
		if (!valid) {
			$input.addClass('invalid');
			$message.addClass('error-message');
			$message.html('Invalid email address.');
			return;
		}
		$parent.css('opacity', '0.5');

		var data = {
			action: 'charitypress_add_subscriber',
			nonce : nonce,
			email : email
		};

		$.post(ajaxurl, data, function (response) {
			$parent.css('opacity', '1');
			if (response.success) {
				$message.removeClass('error-message');
				if (response.data && response.data.message) {
					$message.html(response.data.message);
				} else {
					$message.html('Successfully added.');
				}

				$input.val('');

			} else if (!response.success) {
				$message.html(response.data.message);
				$message.addClass('error-message');
			}

		});


	});

	$('#suggested-team').on('keypress', function (e) {

		if (e.which == 13) {

			$('#suggest-team-execute').click();

		}
	});

	Stripe.setPublishableKey(stripejs.public_key);

});

