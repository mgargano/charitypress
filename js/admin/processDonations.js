/*global jQuery, ajaxurl, chargeUsers, chargeUsers.nonce_get_data*/

jQuery(document).ready(function ($) {
	'use strict';
	var startDate = new Date(),
		endDate = new Date(),
		$datepickerBegin = $('#datePickerBegin'),
		$datepickerEnd = $('#datePickerEnd'),
		$spinner = $('.spinner'),
		offset = chargeUsers['length'],
		$response = $('.wrap.charge-users .response'),
		allChargesDeferred = $.Deferred(),
		allEmailsDeferred = $.Deferred(),
		iterator = 0,
		chargeObjects,
		numberIteration,
		updateData = {
			action  : 'charitypress_set_last_updated',
			security: chargeUsers.nonce_get_data
		},
		lastObject,
		charge = function (object, forceCharge) {
			var dateBegin = $datepickerBegin.datepicker('getDate') / 1000,
				nounsInPeriod = $('#nounsInPeriod').val(),
				chargeData = {
					action        : 'charitypress_charge_user_periodic',
					security      : chargeUsers.nonce_get_data,
					charge_amount : object.charge_amount,
					charge_text   : object.charge_text,
					customer_token: object.customer_token,
					log_note      : object.log_note,
					log_summary   : object.log_summary,
					user_id       : object.user.ID,
					index         : iterator,
					nounsInPeriod : nounsInPeriod,
					date_begin    : dateBegin,
					force         : forceCharge
				};
			iterator++;

			$.ajax({

				type   : 'POST',
				url    : ajaxurl,
				data   : chargeData,
				success: function (response) {
					$response.append(response.statusMessage + '<hr>');
					numberIteration++;
					if (lastObject === numberIteration) {
						allChargesDeferred.resolve();
					} else {
						charge(chargeObjects[iterator], forceCharge);
					}
				},
				async  : true
			});
		},
		kickOffCharges = function (type) {
			$('.safe').hide();
			$('.unsafe').show();
			var nounsInPeriod = $('#nounsInPeriod').val(),
				forceCharge = false,
				data = {
					action       : 'charitypress_build_charges',
					security     : chargeUsers.nonce_get_data,
					nounsInPeriod: nounsInPeriod
				};
			if ('reprocess-queue' == type) {
				forceCharge = true;
				data = {
					action  : 'charitypress_reprocess_queue',
					security: chargeUsers.nonce_get_data
				};
				var fixQueueData = {
					action  : 'charitypress_fix_queue',
					security: chargeUsers.nonce_get_data
				};
				$.ajax({

					type : 'POST',
					url  : ajaxurl,
					data : fixQueueData,
					async: false
				});
			}


			$.post(ajaxurl, data, function (response) {

				numberIteration = 0;
				lastObject = _.size(response);

				if (_.size(response)) {
					chargeObjects = response;
					charge(response[iterator], forceCharge);


				} else {
					$response.append('No users to charge<hr>');
					allChargesDeferred.resolve();
				}
			});
		};


	if (offset === 'monthly') {
		startDate.setMonth(startDate.getMonth() - 1);
	} else {
		startDate.setDate(startDate.getDate() - 7);
	}


	$datepickerBegin.datepicker({
		defaultDate: startDate
	});
	$datepickerBegin.datepicker('setDate', startDate);

	$datepickerEnd.datepicker({
		defaultDate: endDate
	});

	$datepickerEnd.datepicker('setDate', endDate);

	$('#process-donations, #reprocess-queue').on('click', function () {

		var nounsInPeriod = $('#nounsInPeriod').val(),
			dateBegin = $('#datePickerBegin').datepicker('getDate') / 1000,
			$lastRanTimestamp = $('#last-ran-timestamp'),
			lastRanTimestamp = $lastRanTimestamp.val();

		if (!$(this).is('#reprocess-queue')) {
			if (isNaN(nounsInPeriod) || !nounsInPeriod.trim()) {
				var labelContent = $('label[for="nounsInPeriod"]').html();
				alert('Enter in a valid number for ' + labelContent + '.');
				return;
			}
			if (lastRanTimestamp > dateBegin) {
				if (!confirm('This appears to have been run after the start date, are you sure you want to kick off the charging process?')) {
					return;
				}
			}
		}
		$(this).attr('disabled', 'disabled');
		$spinner.show();
		var type = '';
		if ($(this).is('#reprocess-queue')) {
			type = 'reprocess-queue';
		}
		kickOffCharges(type);

	});

	// EMAIL START //

	allChargesDeferred.done(function () {
		var iterator = 0,
			emailObjects,
			numberIteration,
			lastObject;
		$response.append('Charges complete<hr>');
		var email = function (object) {


				var chargeData = {
					action  : 'charitypress_send_email_periodic',
					security: chargeUsers.nonce_get_data,
					to      : object.to,
					subject : object.subject,
					body    : object.body,
					user_id : object.id,
					index   : iterator
				};
				iterator++;

				$.ajax({

					type   : 'POST',
					url    : ajaxurl,
					data   : chargeData,
					success: function (response) {
						$response.append(response.statusMessage + '<hr>');
						numberIteration++;
						if (lastObject === numberIteration) {
							allEmailsDeferred.resolve();
						} else {
							email(emailObjects[iterator]);
						}
					},
					async  : true
				});


			},
			rebuildTotals = function () {
				var rebuildTotalsData = {
					action : 'charitypress_rebuild_totals',
					security: chargeUsers.nonce_get_data
				};
				$.post(ajaxurl, rebuildTotalsData, function(response){
					kickOffEmails();
				});

			},
			kickOffEmails = function () {
				var sendEmailsData = {
					action  : 'charitypress_get_periodic_emails',
					security: chargeUsers.nonce_get_data
				};
				$.post(ajaxurl, sendEmailsData, function (response) {
					numberIteration = 0;
					lastObject = _.size(response);
					if (_.size(response)) {
						$response.append('Sending emails<hr>');
						emailObjects = response;
						email(emailObjects[iterator]);


					} else {
						$response.append('No emails to send<hr>');
						allEmailsDeferred.resolve();
					}
				});
			};

		rebuildTotals();


	});

// EMAIL DONE //

	allEmailsDeferred.done(function () {

		$response.append('<h2>Charges complete.</h2>');
		$.post(ajaxurl, updateData);
		$spinner.hide();
		$('.safe').show();
		$('.unsafe').hide();


	});

})
;