/*global jQuery */

jQuery(document).ready(function ($) {
	$('#suggest-team-execute').on('click', function () {

		var $suggested = $('#suggested-team'),
			suggested = $suggested.val(),
			$status = $(".suggest-status"),
			user_id = $suggested.data('user-id');
		if (!suggested.trim()) {
			$status.html('Please enter a team name to request');
			return;
		}
		var data = {
			'action'   : 'charitypress_suggest_team',
			'suggested': suggested,
			'user_id'  : user_id,
			'security' : $('#suggest-team-nonce').val()
		};
		$status.html('Submitting...');

		$.post(ajaxurl, data, function (response) {

			$status.html(response.data.message);

		});

	});

	$('.suggest-team').on('click', '.action', function () {

		var type = 'accept',
			$parent = $(this).closest('tr'),
			index = $parent.data('key'),
			$spinner = $parent.find('.spinner'),
			$status = $spinner.closest('td');

		if ($spinner.is(':visible') || $spinner.length === 0) {
			return;
		}
		if ($(this).hasClass('reject')) {
			type = 'reject';
		}
		$spinner.show();
		var data = {
			'action'  : 'charitypress_suggested_team_action',
			'type'    : type,
			'index'   : index,
			'security': $('#suggest-team-nonce').val()

		};
		$.post(ajaxurl, data, function (response) {
			$spinner.remove();
			$status.html(response.data.message);
			if ( response.status ){
				$parent.fadeOut();
			}

		});


	});
});