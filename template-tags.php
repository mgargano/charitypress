<?php

function charitypress_get_number_participants() {

	$totals = Charity_Totals::get_totals();

	return (int) count( $totals['users'] );

}

function charitypress_number_participants() {

	echo wp_kses_post( charitypress_get_number_participants() );

}


function charitypress_get_total() {

	$totals = Charity_Totals::get_totals();

	return number_format( (float) $totals['total'], 2 );

}

function charitypress_total() {

	echo wp_kses_post( charitypress_get_total() );

}

function charitypress_get_individual_leaderboard() {

	$totals = Charity_Totals::get_totals();
	$users  = ( $totals['users'] );
	arsort( $users );
	$output   = '';
	$iterator = 1;
	foreach ( $users as $user_id => $total_donated ) {
		if ( get_user_meta( $user_id, 'anonymous', true ) ) {
			continue;
		}
		$user = get_user_by( 'id', $user_id );
		$output .= sprintf( '<div class="user">%s</div><div class="total">%s . %s</div>', wp_kses_post( $user->data->display_name ), (int) $iterator, number_format( (float) $total_donated, 2 ) );
		$iterator ++;
	}

	return $output;

}

function charitypress_individual_leaderboard() {

	echo charitypress_get_individual_leaderboard(); //escaped in the output of charitypress_get_individual_leaderboard function

}

function charitypress_get_team_leaderboard() {

	$totals = Charity_Totals::get_totals();
	$teams  = ( $totals['teams'] );
	arsort( $teams );
	$output   = '';
	$iterator = 1;
	foreach ( $teams as $team_slug => $total_donated ) {

		$team = get_term_by( 'slug', $team_slug, Charity_Team_User_Taxonomy::TAXONOMY_NAME );
		$output .= sprintf( '<div class="team">%s</div><div class="total">%s . %s</div>', wp_kses_post( $team->name ), (int) $iterator, number_format( (float) $total_donated, 2 ) );
		$iterator ++;
	}

	return $output;

}

function charitypress_team_leaderboard() {

	echo charitypress_get_team_leaderboard(); //escaped in the output of charitypress_get_team_leaderboard function

}