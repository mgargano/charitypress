<?php
/*
	Plugin Name: CharityPress
	Plugin URI: http://statenweb.com
	Description: Set up a charity based on points or another metric and sponsorships
	Author: StatenWeb and Mat Gargano
	Version: 0.1
*/

require_once __DIR__ . '/lib/external/titan-framework/titan-framework-embedder.php';
require_once __DIR__ . '/lib/external/autoload.php';

add_action( 'after_setup_theme', 'charity_includes' );

/**
 * Set up includes, after setup theme so that titan is already embedded
 */
function charity_includes() {
	if ( ! class_exists( 'TitanFramework' ) ) {
		return;
	}
	require_once __DIR__ . '/lib/class-charity-ajax.php';
	require_once __DIR__ . '/lib/class-charity-alerts.php';
	require_once __DIR__ . '/lib/class-charity-user-alerts.php';
	require_once __DIR__ . '/lib/class-charity-email.php';
	require_once __DIR__ . '/lib/class-charity-post-logger.php';
	require_once __DIR__ . '/lib/class-charity-post-logger-post-type.php';
	require_once __DIR__ . '/lib/class-charity-role-manager.php';
	require_once __DIR__ . '/lib/class-charity-settings.php';
	require_once __DIR__ . '/lib/class-charity-shortcodes.php';
	require_once __DIR__ . '/lib/class-charity-stripe.php';
	require_once __DIR__ . '/lib/class-charity-stripe-js.php';
	require_once __DIR__ . '/lib/class-charity-team-user-taxonomy.php';
	require_once __DIR__ . '/lib/class-charity-user.php';
	require_once __DIR__ . '/lib/class-charity-user-columns.php';
	require_once __DIR__ . '/lib/class-charity-user-logger.php';
	require_once __DIR__ . '/lib/class-charity-user-profile-fields.php';
	require_once __DIR__ . '/lib/class-charity-charge-screen.php';
	require_once __DIR__ . '/lib/class-charity-totals.php';
	require_once __DIR__ . '/lib/class-charity-email-queue.php';
	require_once __DIR__ . '/lib/class-charity-charge-queue.php';
	require_once __DIR__ . '/lib/class-charity-queue-status.php';
	require_once __DIR__ . '/lib/class-charity-subscriber.php';
	require_once __DIR__ . '/lib/class-charity-email-csv-endpoint.php';
	require_once __DIR__ . '/lib/class-charity-suggest-team-name.php';
	require_once __DIR__ . '/lib/class-charity-user-menus.php';
	require_once __DIR__ . '/lib/class-charity-user-team-menu.php';
	require_once __DIR__ . '/template-tags.php';


}

add_action( 'wp_enqueue_scripts', 'charitypress_enqueues' );
add_action( 'init', 'charitypress_set_stripe_key' );
register_activation_hook( __FILE__, function () {
	flush_rewrite_rules();
} );

register_deactivation_hook( __FILE__, function () {
	flush_rewrite_rules();
} );

function charitypress_enqueues() {

	wp_enqueue_script( 'jquery-validation-engine', plugins_url( 'js/lib/external/jquery.validationEngine.js', __FILE__ ), array(
		'jquery'
	) );

	wp_enqueue_script( 'charitypress', plugins_url( 'js/charitypress.js', __FILE__ ), array(
		'jquery',
		'stripejs',
		'jquery-validation-engine'
	) );

	wp_enqueue_style( 'charitypress', plugins_url( 'css/charitypress.css', __FILE__ ) );
	wp_enqueue_style( 'jquery-validation-engine', plugins_url( 'css/external/validationEngine.jquery.css', __FILE__ ) );

	wp_localize_script( 'charitypress', 'charitypress', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );

}

function charitypress_set_stripe_key() {
	if ( ! class_exists( 'Charity_Stripe' ) ) {
		return;
	}
	$cs         = new Charity_Stripe;
	$stripe_key = $cs->get_private_key();
	Stripe::setApiKey( $stripe_key );
}


